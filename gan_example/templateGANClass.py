from __future__ import print_function, division
from matplotlib import ticker
from sklearn import preprocessing
import uproot
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import sys
import toolsGANClass as tlG


class GAN():
    def __init__(self):
        np.random.seed(23)

        self.nameFile = "/home/victor/Documents/CEA/oldsample/ntuples/"
        self.nameTree = "tthHadronicTagDumper/trees/tth_13TeV_all"
        self.features = ['pho1_idmva', 'pho1_pt', 'pho1_sceta', 'nvtx']

        self.latent_dim = 1
        self.real_feat_dim = len(self.features) - 1
        self.genModelName = "genModel_" + str(self.latent_dim) + ".h5"
        self.discModelName = "discModel_" + str(self.latent_dim) + ".h5"
        # here is a nice place define combined model

    #    def generatorBuild(self):
    ##Here you can put the generator model

    #    def discriminatorBuild(self):
    ##Similarly discriminator can be built here

    def train(self, epochs, batch_size=256, sample_interval=100):
        df, df_norm, df_weight = self.openRootFiles()
        df_train = df_norm.sample(frac=0.75, random_state=0)
        print(df_train.index)
        df_weight_train = df_weight[df_train.index]
        df_weight_test = df_weight.drop(df_train.index)
        df_test = df_norm.drop(df_train.index)
        # Load the dataset
        X_train = df_train.to_numpy()
        X_test = df_test.to_numpy()

        X_weight_train = df_weight_train.to_numpy()
        X_weight_test = df_weight_train.to_numpy()
        correlations = df_test.corr()

        # plot correlation matrix
        figCorr = plt.figure()
        ax = figCorr.add_subplot(111)
        cax = ax.matshow(correlations, vmin=-0.2, vmax=0.2, )
        tlG.annotate_heatmap(cax)
        figCorr.colorbar(cax)
        ticks = np.arange(0, len(self.features), 1)

        ax.set_xticks(ticks)
        ax.set_yticks(ticks)
        ax.set_xticklabels(self.features)
        ax.set_yticklabels(self.features)
        plt.savefig(r"images/corrReal.png")
        plt.close()
        print('train array')
        print(X_train)
        print('photon ID')
        print(X_train[:, 0])
        i = 1
        self.plotFeatures2D(X_test, 'real', df_weight.drop(df_train.index))

        for epoch in range(epochs):
            ## Generate samples

            if epoch % sample_interval == 0:
                print("you may try to print your results once in a while to visualize the progress")

            ## Train the discriminator 
            ## Train the generator

    def evaluate(self):
        df, df_norm, df_weight = self.openRootFiles()

    def plotFeatures2D(self, arrSamp, title, df_weight):
        i = 1
        xAxis = np.linspace(-1, 1, num=20)
        yAxis = np.linspace(-1, 1, num=20)

        while i < len(self.features):
            yAxis = np.linspace(-1, 1, num=20)
            hist, edgesx, edgesy = np.histogram2d(arrSamp[:, 0], arrSamp[:, i], bins=(xAxis, yAxis), weights=df_weight)
            plt.pcolormesh(edgesx, edgesy, hist.T, cmap='YlOrRd')
            plt.xlabel(self.features[0])
            plt.ylabel(self.features[i])
            plt.savefig(r"images/2D%s_%d.png" % (title, i))
            plt.close()
            i += 1

    def openRootFiles(self):
        gJet = uproot.open(self.nameFile)[self.nameTree]
        df = gJet.pandas.df(self.features + ['weight'])
        df_weight = df['weight']
        df = df.drop('weight', axis=1)
        df_norm = (df - df.mean()) * 2 / (df.max() - df.min())
        return df, df_norm, df_weight


if __name__ == '__main__':
    gan = GAN()
    gan.train(epochs=10000, batch_size=256, sample_interval=200)
#    gan.evaluate()
