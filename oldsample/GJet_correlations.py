import os
import matplotlib.pyplot as plt
import pandas
import lib

my_dpi = 96

""" Global variables """
luminosity = 41.5

""" Main """
df_gjet = pandas.DataFrame()

# data extraction
completion = 0
print("Extracting data :")
for folder in os.scandir("ntuples"):
    for filename in os.scandir(folder):
        completion += 1
        print(int((completion / 69) * 100), "%")
        if 'GJets_' in folder.name and 'Fall17' in folder.name:
            if '40To100' in folder.name:
                print("Extracting from " + folder.name)
                scale1fb = 3.346080992393051
                df_gjet = lib.extract(df_gjet, filename, scale1fb * luminosity)
            elif '100To200' in folder.name:
                print("Extracting from " + folder.name)
                scale1fb = 0.86681778146470212
                df_gjet = lib.extract(df_gjet, filename, scale1fb * luminosity)
            elif '200To400' in folder.name:
                print("Extracting from " + folder.name)
                scale1fb = 0.11795314318135013
                df_gjet = lib.extract(df_gjet, filename, scale1fb * luminosity)
            elif '400To600' in folder.name:
                print("Extracting from " + folder.name)
                scale1fb = 0.055537261041074731
                df_gjet = lib.extract(df_gjet, filename, scale1fb * luminosity)
            elif '600ToInf' in folder.name:
                print("Extracting from " + folder.name)
                scale1fb = 0.022394866068735982
                df_gjet = lib.extract(df_gjet, filename, scale1fb * luminosity)
print("Done !")

# apply cuts
lib.cuts(df_gjet)

# compute correlation matrix
obs_final = ['MinPhotonID', 'photonPt_fake', 'photonEta_fake', 'n_jets', 'nvtx', 'diphoPt:mass']

w = df_gjet['weight'].to_numpy()
df_gjet = df_gjet[obs_final]
correlation = df_gjet.corr()

# plot correlations
savepath = 'plots/correlations/'

plt.figure(1, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
im, _ = lib.heatmap(correlation, obs_final, obs_final, origin='lower')
lib.annotate_heatmap(im)
plt.savefig(savepath + 'GJet_2017_correlations.png')

ranges = [[0, 200], [-2.5, 2.5], [-0.5, 14.5], [0, 70], [0, 2]]
for i in range(1, len(obs_final)):
    plt.figure(i+1, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
    plt.hist2d(df_gjet['MinPhotonID'], df_gjet[obs_final[i]], bins=15,
               range=[[-0.9, 1], [ranges[i-1][0], ranges[i-1][1]]], weights=w)
    plt.xlabel('Fake photonID')
    plt.ylabel(obs_final[i])
    plt.colorbar()
    plt.savefig(savepath + 'GJet_2017_correlations_MinPhotonIDvs%s.png' % (obs_final[i]))

plt.show()
