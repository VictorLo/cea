import os
import numpy as np
import matplotlib.pyplot as plt
import uproot as up
import pandas
import ecalic.cmsStyle

my_dpi = 96

""" Global variables """
luminosity = 41.5
branchpath = "tthHadronicTagDumper/trees/tth_13TeV_all"
observables = ['leadIDMVA', 'subleadIDMVA', 'leadPt', 'subleadPt', 'leadEta', 'subleadEta',
               'leadGenMatch', 'subleadGenMatch', 'n_jets', 'nb_loose', 'mass', 'weight']

""" Functions """


def extract(dataframe, file, scalefb=1.0, branch=branchpath, features=None):

    if features is None:
        features = observables

    events = up.open(file)[branch]
    tmp_df = events.pandas.df(features)
    tmp_df['weight'] = tmp_df['weight'] * scalefb
    tmp_df['MaxPhotonID'] = tmp_df['leadIDMVA'].where(tmp_df['leadIDMVA'] >= tmp_df['subleadIDMVA'],
                                                      tmp_df['subleadIDMVA'])
    tmp_df['MinPhotonID'] = tmp_df['subleadIDMVA'].where(tmp_df['leadIDMVA'] >= tmp_df['subleadIDMVA'],
                                                         tmp_df['leadIDMVA'])

    return dataframe.append(tmp_df, ignore_index=True)


def cuts(df):
    cutmass = '(mass > 100 and mass < 115) or (mass > 135 and mass < 180)'
    cutpt = 'leadPt > 35 and subleadPt > 25'
    cuteta = 'leadEta > -2.5 and leadEta < 2.5 and subleadEta > -2.5 and subleadEta < 2.5'
    # cutnjets = 'n_jets > 1'
    # cutnbjets = 'nb_loose > 0'
    df.query(cutmass, inplace=True)
    df.query(cutpt, inplace=True)
    df.query(cuteta, inplace=True)
    # df.query(cutnjets, inplace=True)
    # df.query(cutnbjets, inplace=True)
    return df


def gjet_tagging(dataframes):
    df = dataframes[0].query('(leadGenMatch != 1 and subleadGenMatch == 1) or '
                             '(leadGenMatch == 1 and subleadGenMatch != 1)')
    for i in range(1, len(dataframes)):
        tmp_df = dataframes[i].query('(leadGenMatch != 1 and subleadGenMatch == 1) or '
                                     '(leadGenMatch == 1 and subleadGenMatch != 1)')
        df = df.append(tmp_df, ignore_index=True)
    return df


def jets_tagging(dataframes):
    df = dataframes[0].query('leadGenMatch != 1 and subleadGenMatch != 1')
    for i in range(1, len(dataframes)):
        tmp_df = dataframes[i].query('leadGenMatch != 1 and subleadGenMatch != 1')
        df = df.append(tmp_df, ignore_index=True)
    return df


""" Main """
df_data = pandas.DataFrame(columns=observables + ['MaxPhotonID', 'MinPhotonID'])
df_gg = pandas.DataFrame(columns=observables + ['MaxPhotonID', 'MinPhotonID'])
df_gjet = pandas.DataFrame(columns=observables + ['MaxPhotonID', 'MinPhotonID'])
df_jets = pandas.DataFrame(columns=observables + ['MaxPhotonID', 'MinPhotonID'])

# data extraction
completion = 0
print("Extracting data :")
for folder in os.scandir("ntuples"):
    for filename in os.scandir(folder):
        completion += 1
        print(int((completion / 69) * 100), "%")
        if 'DoubleEG_Run2017' in folder.name or 'DoubleEG_Run2016' in folder.name:
            print("Extracting from " + folder.name)
            df_data = extract(df_data, filename)
        elif 'DiPhoton' in folder.name:
            if 'Summer16' in folder.name:
                if 'DiPhotonJets_' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.0000135911
                    df_gg = extract(df_gg, filename, scale1fb * luminosity)
                elif 'DiPhotonJetsBox_M40_80' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.0833888190
                    df_gg = extract(df_gg, filename, scale1fb * luminosity)
                elif 'DiPhotonJetsBox_MGG-80toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.0030388418
                    df_gg = extract(df_gg, filename, scale1fb * luminosity)
            elif 'Fall17' in folder.name:
                if 'DiPhotonJets_' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.0001215319
                    df_gg = extract(df_gg, filename, scale1fb * luminosity)
                elif 'DiPhotonJetsBox_M40_80' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.1103250733
                    df_gg = extract(df_gg, filename, scale1fb * luminosity)
                elif 'DiPhotonJetsBox_MGG-80toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.0038391582
                    df_gg = extract(df_gg, filename, scale1fb * luminosity)
                elif 'DiPhotonJetsBox1BJet_MGG-80toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.0048210001
                    df_gg = extract(df_gg, filename, scale1fb * luminosity)
                elif 'DiPhotonJetsBox2BJets_MGG-80toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.0029509609
                    df_gg = extract(df_gg, filename, scale1fb * luminosity)
        elif 'GJet_' in folder.name:
            if 'Summer16' in folder.name:
                if '20to40' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.0087894014
                    df_gjet = extract(df_gjet, filename, scale1fb * luminosity)
                elif '20toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 2.1152592672
                    df_gjet = extract(df_gjet, filename, scale1fb * luminosity)
                elif '40toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.0117622870
                    df_gjet = extract(df_gjet, filename, scale1fb * luminosity)
            elif 'Fall17' in folder.name:
                if '20to40' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.0131740441
                    df_gjet = extract(df_gjet, filename, scale1fb * luminosity)
                elif '20toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.0792571816
                    df_gjet = extract(df_gjet, filename, scale1fb * luminosity)
                elif '40toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.0110810550
                    df_gjet = extract(df_gjet, filename, scale1fb * luminosity)
        elif 'QCD' in folder.name:
            if 'Summer16' in folder.name:
                if '30to40' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 1.6057653285
                    df_jets = extract(df_jets, filename, scale1fb * luminosity)
                if '30toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 5.7410435024360789
                    df_jets = extract(df_jets, filename, scale1fb * luminosity)
                if '40toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 5.4476833126
                    df_jets = extract(df_jets, filename, scale1fb * luminosity)
            elif 'Fall17' in folder.name:
                if '30to40' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 1.6999467268
                    df_jets = extract(df_jets, filename, scale1fb * luminosity)
                if '30toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 5.7511203845
                    df_jets = extract(df_jets, filename, scale1fb * luminosity)
                if '40toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 6.2048662964
                    df_jets = extract(df_jets, filename, scale1fb * luminosity)
print("Done !")

# apply cuts
cuts(df_data)

cuts(df_gg)
cuts(df_gjet)
cuts(df_jets)

# select photonID region
select_photonid = 'MaxPhotonID >= -0.2 and MinPhotonID <= -0.2'

df_data_final = df_data.query(select_photonid)

df_gg_final = df_gg.query(select_photonid)
df_gjet_region = df_gjet.query(select_photonid)
df_jets_region = df_jets.query(select_photonid)

# tagging of events
df_gjet_final = gjet_tagging([df_gjet_region, df_jets_region])
df_jets_final = jets_tagging([df_gjet_region, df_jets_region])

# convert weight from dataframes to arrays
data_w = df_data_final['weight'].T.to_numpy()
bkg_w = np.array([df_gg_final['weight'].T.to_numpy(), df_gjet_final['weight'].T.to_numpy(),
                  df_jets_final['weight'].T.to_numpy()])
scale = (data_w.sum()) / (bkg_w[0].sum() + bkg_w[1].sum() + bkg_w[2].sum())

# plot histograms
bkg_labels = ['DiPhoton', 'GJet', 'Multijets']

plt.figure(1, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.subplot(121)
plt.hist([df_gg_final['MaxPhotonID'].T.to_numpy(),
          df_gjet_final['MaxPhotonID'].T.to_numpy(),
          df_jets_final['MaxPhotonID'].T.to_numpy()],
         20, weights=bkg_w * scale, stacked=True, label=bkg_labels, zorder=-1)
n, bin_edges = np.histogram(df_data_final['MaxPhotonID'], 20, weights=data_w)
bin_centres = (bin_edges[:-1] + bin_edges[1:])*0.5
plt.scatter(bin_centres, n, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('ID MVA')
plt.ylabel('Events')
plt.legend(loc='upper left')
plt.title('Photons with MAX photonID')
plt.subplot(122)
plt.hist([df_gg_final['MinPhotonID'].T.to_numpy(),
          df_gjet_final['MinPhotonID'].T.to_numpy(),
          df_jets_final['MinPhotonID'].T.to_numpy()],
         20, weights=bkg_w * scale, stacked=True, label=bkg_labels, zorder=-1)
n1, bin_edges1 = np.histogram(df_data_final['MinPhotonID'], 20, weights=data_w)
bin_centres1 = (bin_edges1[:-1] + bin_edges1[1:])*0.5
plt.scatter(bin_centres1, n1, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('ID MVA')
plt.ylabel('Events')
plt.legend(loc='upper right')
plt.title('Photons with MIN photonID')
plt.savefig('plots/AllMCvsData/Method1/MaxAbove-0.2_MinBelow-0.2/AllMCvsData_1_16+17_photonID.png')

plt.figure(2, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.subplot(121)
plt.hist([np.clip(df_gg_final['leadPt'].T.to_numpy(), None, 201),
          np.clip(df_gjet_final['leadPt'].T.to_numpy(), None, 201),
          np.clip(df_jets_final['leadPt'].T.to_numpy(), None, 201)],
         21, weights=bkg_w * scale, stacked=True, label=bkg_labels, zorder=-1)
n2, bin_edges2 = np.histogram(np.clip(df_data_final['leadPt'], None, 201), 21, weights=data_w)
bin_centres2 = (bin_edges2[:-1] + bin_edges2[1:])*0.5
plt.scatter(bin_centres2, n2, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('pT [GeV]')
plt.ylabel('Events')
# plt.yscale('log')
plt.legend()
plt.title('Photons with lead pT')
plt.subplot(122)
plt.hist([np.clip(df_gg_final['subleadPt'].T.to_numpy(), None, 201),
          np.clip(df_gjet_final['subleadPt'].T.to_numpy(), None, 201),
          np.clip(df_jets_final['subleadPt'].T.to_numpy(), None, 201)],
         21, weights=bkg_w * scale, stacked=True, label=bkg_labels, zorder=-1)
n3, bin_edges3 = np.histogram(np.clip(df_data_final['subleadPt'], None, 201), 21, weights=data_w)
bin_centres3 = (bin_edges3[:-1] + bin_edges3[1:])*0.5
plt.scatter(bin_centres3, n3, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('pT [GeV]')
plt.ylabel('Events')
# plt.yscale('log')
plt.legend()
plt.title('Photons with sublead pT')
plt.savefig('plots/AllMCvsData/Method1/MaxAbove-0.2_MinBelow-0.2/AllMCvsData_1_16+17_pT.png')

plt.figure(3, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.subplot(121)
plt.hist([df_gg_final['leadEta'].T.to_numpy(),
          df_gjet_final['leadEta'].T.to_numpy(),
          df_jets_final['leadEta'].T.to_numpy()],
         20, weights=bkg_w * scale, stacked=True, label=bkg_labels, zorder=-1)
n4, bin_edges4 = np.histogram(df_data_final['leadEta'], 20, weights=data_w)
bin_centres4 = (bin_edges4[:-1] + bin_edges4[1:])*0.5
plt.scatter(bin_centres4, n4, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('eta')
plt.ylabel('Events')
plt.legend()
plt.title('Photons with lead pT')
plt.subplot(122)
plt.hist([df_gg_final['subleadEta'].T.to_numpy(),
          df_gjet_final['subleadEta'].T.to_numpy(),
          df_jets_final['subleadEta'].T.to_numpy()],
         20, weights=bkg_w * scale, stacked=True, label=bkg_labels, zorder=-1)
n5, bin_edges5 = np.histogram(df_data_final['subleadEta'], 20, weights=data_w)
bin_centres5 = (bin_edges5[:-1] + bin_edges5[1:])*0.5
plt.scatter(bin_centres5, n5, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('eta')
plt.ylabel('Events')
plt.legend()
plt.title('Photons with sublead pT')
plt.savefig('plots/AllMCvsData/Method1/MaxAbove-0.2_MinBelow-0.2/AllMCvsData_16+17_1_eta.png')

plt.figure(4)
plt.hist([df_gg_final['n_jets'].T.to_numpy(),
          df_gjet_final['n_jets'].T.to_numpy(),
          df_jets_final['n_jets'].T.to_numpy()],
         7, range=(0.5, 7.5), weights=bkg_w * scale, stacked=True, label=bkg_labels, zorder=-1)
n6, bin_edges6 = np.histogram(df_data_final['n_jets'].to_numpy(), 7, range=(0.5, 7.5), weights=data_w)
bin_centres6 = (bin_edges6[:-1] + bin_edges6[1:])*0.5
plt.scatter(bin_centres6, n6, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('Number of jets')
plt.ylabel('Events')
# plt.yscale('log')
plt.legend(loc='upper right')
plt.savefig('plots/AllMCvsData/Method1/MaxAbove-0.2_MinBelow-0.2/AllMCvsData_16+17_1_njets.png')

plt.figure(5)
plt.hist([df_gg_final['nb_loose'].T.to_numpy(),
          df_gjet_final['nb_loose'].T.to_numpy(),
          df_jets_final['nb_loose'].T.to_numpy()],
         5, range=(-0.5, 4.5), weights=bkg_w * scale, stacked=True, label=bkg_labels, zorder=-1)
n6, bin_edges6 = np.histogram(df_data_final['nb_loose'].to_numpy(), 5, range=(-0.5, 4.5), weights=data_w)
bin_centres6 = (bin_edges6[:-1] + bin_edges6[1:])*0.5
plt.scatter(bin_centres6, n6, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('Number of b jets')
plt.ylabel('Events')
# plt.yscale('log')
plt.legend(loc='upper right')
plt.savefig('plots/AllMCvsData/Method1/MaxAbove-0.2_MinBelow-0.2/AllMCvsData_16+17_1_nBjets.png')

plt.show()
