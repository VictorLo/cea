import os
import numpy as np
import matplotlib.pyplot as plt
import uproot as up
import pandas

my_dpi = 96

luminosity = 41.5

features = ['leadIDMVA', 'subleadIDMVA', 'leadPt', 'subleadPt', 'leadEta', 'subleadEta', 'weight',
            'n_jets', 'nb_loose', 'mass']
df_data = pandas.DataFrame(columns=features)
df_gg = pandas.DataFrame(columns=features)
df_gjet = pandas.DataFrame(columns=features)
df_jets = pandas.DataFrame(columns=features)

# data extraction
completion = 0
print("Extracting data :")
for folder in os.scandir("ntuples"):
    for filename in os.scandir(folder):
        completion += 1
        print(int((completion / 69) * 100), "%")
        if 'DoubleEG_Run2017' in folder.name:
            print("Extracting from " + folder.name)
            events = up.open(filename)["tthHadronicTagDumper/trees/tth_13TeV_all"]
            tmp_df = events.pandas.df(features)
            df_data = df_data.append(tmp_df, ignore_index=True)
        elif 'DiPhotonJets_' in folder.name and 'Fall17' in folder.name:
            print("Extracting from " + folder.name)
            events = up.open(filename)["tthHadronicTagDumper/trees/tth_13TeV_all"]
            tmp_df = events.pandas.df(features)
            scale1fb = 0.00012076676328613451
            tmp_df['weight'] = tmp_df['weight'] * scale1fb * luminosity
            df_gg = df_gg.append(tmp_df, ignore_index=True)
        elif 'GJet' in folder.name and 'Fall17' in folder.name and 'HT' not in folder.name:
            print("Extracting from " + folder.name)
            events = up.open(filename)["tthHadronicTagDumper/trees/tth_13TeV_all"]
            tmp_df = events.pandas.df(features)
            if '20to40' in folder.name:
                scale1fb = 0.013174044112514596
                tmp_df['weight'] = tmp_df['weight'] * scale1fb * luminosity
                df_gjet = df_gjet.append(tmp_df, ignore_index=True)
            if '20toInf' in folder.name:
                scale1fb = 0.075237801398417098
                tmp_df['weight'] = tmp_df['weight'] * scale1fb * luminosity
                df_gjet = df_gjet.append(tmp_df, ignore_index=True)
            if '40toInf' in folder.name:
                scale1fb = 0.011081055034051624
                tmp_df['weight'] = tmp_df['weight'] * scale1fb * luminosity
                df_gjet = df_gjet.append(tmp_df, ignore_index=True)
        elif 'QCD' in folder.name and 'Fall17' in folder.name:
            print("Extracting from " + folder.name)
            events = up.open(filename)["tthHadronicTagDumper/trees/tth_13TeV_all"]
            tmp_df = events.pandas.df(features)
            if '30to40' in folder.name:
                scale1fb = 1.6912594606743319
                tmp_df['weight'] = tmp_df['weight'] * scale1fb * luminosity
                df_jets = df_jets.append(tmp_df, ignore_index=True)
            if '30toInf' in folder.name:
                scale1fb = 5.7410435024360789
                tmp_df['weight'] = tmp_df['weight'] * scale1fb * luminosity
                df_jets = df_jets.append(tmp_df, ignore_index=True)
            if '40toInf' in folder.name:
                scale1fb = 5.7268841667121686
                tmp_df['weight'] = tmp_df['weight'] * scale1fb * luminosity
                df_jets = df_jets.append(tmp_df, ignore_index=True)
print("Done !")

# sort the photons by photonID value not by pT
df_data_leadmaxid = df_data[(df_data['leadIDMVA'] >= df_data['subleadIDMVA']) & (df_data['n_jets'] > 1) &
                            (df_data['nb_loose'] > 0) & (((df_data['mass'] > 100) & (df_data['mass'] < 115)) |
                                                         ((df_data['mass'] > 135) & (df_data['mass'] < 180)))]
df_data_subleadmaxid = df_data[(df_data['leadIDMVA'] < df_data['subleadIDMVA']) & (df_data['n_jets'] > 1) &
                               (df_data['nb_loose'] > 0) & (((df_data['mass'] > 100) & (df_data['mass'] < 115)) |
                                                            ((df_data['mass'] > 135) & (df_data['mass'] < 180)))]

data_maxid = np.concatenate((df_data_leadmaxid['leadIDMVA'].T.to_numpy(),
                             df_data_subleadmaxid['subleadIDMVA'].T.to_numpy()))
data_minid = np.concatenate((df_data_leadmaxid['subleadIDMVA'].T.to_numpy(),
                             df_data_subleadmaxid['leadIDMVA'].T.to_numpy()))
data_maxid_pt = np.concatenate((df_data_leadmaxid['leadPt'].T.to_numpy(),
                                df_data_subleadmaxid['subleadPt'].T.to_numpy()))
data_minid_pt = np.concatenate((df_data_leadmaxid['subleadPt'].T.to_numpy(),
                                df_data_subleadmaxid['leadPt'].T.to_numpy()))
data_maxid_eta = np.concatenate((df_data_leadmaxid['leadEta'].T.to_numpy(),
                                 df_data_subleadmaxid['subleadEta'].T.to_numpy()))
data_minid_eta = np.concatenate((df_data_leadmaxid['subleadEta'].T.to_numpy(),
                                 df_data_subleadmaxid['leadEta'].T.to_numpy()))
data_njets = np.concatenate((df_data_leadmaxid['n_jets'].T.to_numpy(), df_data_subleadmaxid['n_jets'].T.to_numpy()))
data_w = np.concatenate((df_data_leadmaxid['weight'].T.to_numpy(), df_data_subleadmaxid['weight'].T.to_numpy()))

df_gg_leadmaxid = df_gg[(df_gg['leadIDMVA'] >= df_gg['subleadIDMVA']) & (df_gg['n_jets'] > 1) &
                        (df_gg['nb_loose'] > 0) & (((df_gg['mass'] > 100) & (df_gg['mass'] < 115)) |
                                                   ((df_gg['mass'] > 135) & (df_gg['mass'] < 180)))]
df_gg_subleadmaxid = df_gg[(df_gg['leadIDMVA'] < df_gg['subleadIDMVA']) & (df_gg['n_jets'] > 1) &
                           (df_gg['nb_loose'] > 0) & (((df_gg['mass'] > 100) & (df_gg['mass'] < 115)) |
                                                      ((df_gg['mass'] > 135) & (df_gg['mass'] < 180)))]
df_gjet_leadmaxid = df_gjet[(df_gjet['leadIDMVA'] >= df_gjet['subleadIDMVA']) & (df_gjet['n_jets'] > 1) &
                            (df_gjet['nb_loose'] > 0) & (((df_gjet['mass'] > 100) & (df_gjet['mass'] < 115)) |
                                                         ((df_gjet['mass'] > 135) & (df_gjet['mass'] < 180)))]
df_gjet_subleadmaxid = df_gjet[(df_gjet['leadIDMVA'] < df_gjet['subleadIDMVA']) & (df_gjet['n_jets'] > 1) &
                               (df_gjet['nb_loose'] > 0) & (((df_gjet['mass'] > 100) & (df_gjet['mass'] < 115)) |
                                                            ((df_gjet['mass'] > 135) & (df_gjet['mass'] < 180)))]
df_jets_leadmaxid = df_jets[(df_jets['leadIDMVA'] >= df_jets['subleadIDMVA']) & (df_jets['n_jets'] > 1) &
                            (df_jets['nb_loose'] > 0) & (((df_jets['mass'] > 100) & (df_jets['mass'] < 115)) |
                                                         ((df_jets['mass'] > 135) & (df_jets['mass'] < 180)))]
df_jets_subleadmaxid = df_jets[(df_jets['leadIDMVA'] < df_jets['subleadIDMVA']) & (df_jets['n_jets'] > 1) &
                               (df_jets['nb_loose'] > 0) & (((df_jets['mass'] > 100) & (df_jets['mass'] < 115)) |
                                                            ((df_jets['mass'] > 135) & (df_jets['mass'] < 180)))]

bkg_maxid = [np.concatenate((df_gg_leadmaxid['leadIDMVA'].T.to_numpy(),
                             df_gg_subleadmaxid['subleadIDMVA'].T.to_numpy())),
             np.concatenate((df_gjet_leadmaxid['leadIDMVA'].T.to_numpy(),
                             df_gjet_subleadmaxid['subleadIDMVA'].T.to_numpy())),
             np.concatenate((df_jets_leadmaxid['leadIDMVA'].T.to_numpy(),
                             df_jets_subleadmaxid['subleadIDMVA'].T.to_numpy()))]
bkg_minid = [np.concatenate((df_gg_leadmaxid['subleadIDMVA'].T.to_numpy(),
                             df_gg_subleadmaxid['leadIDMVA'].T.to_numpy())),
             np.concatenate((df_gjet_leadmaxid['subleadIDMVA'].T.to_numpy(),
                             df_gjet_subleadmaxid['leadIDMVA'].T.to_numpy())),
             np.concatenate((df_jets_leadmaxid['subleadIDMVA'].T.to_numpy(),
                             df_jets_subleadmaxid['leadIDMVA'].T.to_numpy()))]
bkg_maxid_pt = [np.concatenate((df_gg_leadmaxid['leadPt'].T.to_numpy(),
                                df_gg_subleadmaxid['subleadPt'].T.to_numpy())),
                np.concatenate((df_gjet_leadmaxid['leadPt'].T.to_numpy(),
                                df_gjet_subleadmaxid['subleadPt'].T.to_numpy())),
                np.concatenate((df_jets_leadmaxid['leadPt'].T.to_numpy(),
                                df_jets_subleadmaxid['subleadPt'].T.to_numpy()))]
bkg_minid_pt = [np.concatenate((df_gg_leadmaxid['subleadPt'].T.to_numpy(),
                                df_gg_subleadmaxid['leadPt'].T.to_numpy())),
                np.concatenate((df_gjet_leadmaxid['subleadPt'].T.to_numpy(),
                                df_gjet_subleadmaxid['leadPt'].T.to_numpy())),
                np.concatenate((df_jets_leadmaxid['subleadPt'].T.to_numpy(),
                                df_jets_subleadmaxid['leadPt'].T.to_numpy()))]
bkg_maxid_eta = [np.concatenate((df_gg_leadmaxid['leadEta'].T.to_numpy(),
                                 df_gg_subleadmaxid['subleadEta'].T.to_numpy())),
                 np.concatenate((df_gjet_leadmaxid['leadEta'].T.to_numpy(),
                                 df_gjet_subleadmaxid['subleadEta'].T.to_numpy())),
                 np.concatenate((df_jets_leadmaxid['leadEta'].T.to_numpy(),
                                 df_jets_subleadmaxid['subleadEta'].T.to_numpy()))]
bkg_minid_eta = [np.concatenate((df_gg_leadmaxid['subleadEta'].T.to_numpy(),
                                 df_gg_subleadmaxid['leadEta'].T.to_numpy())),
                 np.concatenate((df_gjet_leadmaxid['subleadEta'].T.to_numpy(),
                                 df_gjet_subleadmaxid['leadEta'].T.to_numpy())),
                 np.concatenate((df_jets_leadmaxid['subleadEta'].T.to_numpy(),
                                 df_jets_subleadmaxid['leadEta'].T.to_numpy()))]
bkg_njets = [np.concatenate((df_gg_leadmaxid['n_jets'].T.to_numpy(), df_gg_subleadmaxid['n_jets'].T.to_numpy())),
             np.concatenate((df_gjet_leadmaxid['n_jets'].T.to_numpy(), df_gjet_subleadmaxid['n_jets'].T.to_numpy())),
             np.concatenate((df_jets_leadmaxid['n_jets'].T.to_numpy(), df_jets_subleadmaxid['n_jets'].T.to_numpy()))]
bkg_w = [np.concatenate((df_gg_leadmaxid['weight'].T.to_numpy(), df_gg_subleadmaxid['weight'].T.to_numpy())),
         np.concatenate((df_gjet_leadmaxid['weight'].T.to_numpy(), df_gjet_subleadmaxid['weight'].T.to_numpy())),
         np.concatenate((df_jets_leadmaxid['weight'].T.to_numpy(), df_jets_subleadmaxid['weight'].T.to_numpy()))]

# select only data with g+jet behaviour
select_data_maxid = data_maxid >= 0.5
select_data_minid = data_minid <= -0.2

new_data_maxid = data_maxid[select_data_maxid & select_data_minid]
new_data_minid = data_minid[select_data_maxid & select_data_minid]
new_data_maxid_pt = data_maxid_pt[select_data_maxid & select_data_minid]
new_data_minid_pt = data_minid_pt[select_data_maxid & select_data_minid]
new_data_maxid_eta = data_maxid_eta[select_data_maxid & select_data_minid]
new_data_minid_eta = data_minid_eta[select_data_maxid & select_data_minid]
new_data_njets = data_njets[select_data_maxid & select_data_minid]
new_data_w = data_w[select_data_maxid & select_data_minid]

select_bkg_maxid = [bkg_maxid[i] >= 0.5 for i in range(len(bkg_maxid))]
select_bkg_minid = [bkg_minid[i] <= -0.2 for i in range(len(bkg_minid))]

new_bkg_maxid = [bkg_maxid[i][select_bkg_maxid[i] & select_bkg_minid[i]] for i in range(len(bkg_maxid))]
new_bkg_minid = [bkg_minid[i][select_bkg_maxid[i] & select_bkg_minid[i]] for i in range(len(bkg_minid))]
new_bkg_maxid_pt = [bkg_maxid_pt[i][select_bkg_maxid[i] & select_bkg_minid[i]] for i in range(len(bkg_maxid))]
new_bkg_minid_pt = [bkg_minid_pt[i][select_bkg_maxid[i] & select_bkg_minid[i]] for i in range(len(bkg_minid))]
new_bkg_maxid_eta = [bkg_maxid_eta[i][select_bkg_maxid[i] & select_bkg_minid[i]] for i in range(len(bkg_maxid))]
new_bkg_minid_eta = [bkg_minid_eta[i][select_bkg_maxid[i] & select_bkg_minid[i]] for i in range(len(bkg_minid))]
new_bkg_njets = [bkg_njets[i][select_bkg_maxid[i] & select_bkg_minid[i]] for i in range(len(bkg_maxid))]
new_bkg_w = [bkg_w[i][select_bkg_maxid[i] & select_bkg_minid[i]] for i in range(len(bkg_maxid))]

# plot histograms
plt.figure(1, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.subplot(121)
plt.hist(new_bkg_maxid, 50, weights=new_bkg_w, stacked=True, label=['DiPhoton', 'GJet', 'Multijets'], zorder=-1)
n, bin_edges = np.histogram(new_data_maxid, 50, weights=new_data_w)
bin_centres = (bin_edges[:-1] + bin_edges[1:])*0.5
plt.scatter(bin_centres, n, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('ID MVA')
plt.ylabel('Entries')
plt.legend(loc='upper left')
plt.title('Photons with MAX photonID')
plt.subplot(122)
plt.hist(new_bkg_minid, 50, weights=new_bkg_w, stacked=True, label=['DiPhoton', 'GJet', 'Multijets'], zorder=-1)
n1, bin_edges1 = np.histogram(new_data_minid, 50, weights=new_data_w)
bin_centres1 = (bin_edges1[:-1] + bin_edges1[1:])*0.5
plt.scatter(bin_centres1, n1, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('ID MVA')
plt.ylabel('Entries')
plt.legend(loc='upper right')
plt.title('Photons with MIN photonID')
plt.savefig('plots/AllMCvsData_2017_photonID.png')

plt.figure(2, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.subplot(121)
plt.hist([np.clip(new_bkg_maxid_pt[i], None, 301) for i in range(len(new_bkg_maxid_pt))], 51, weights=new_bkg_w,
         stacked=True, label=['DiPhoton', 'GJet', 'Multijets'], zorder=-1)
n2, bin_edges2 = np.histogram(np.clip(new_data_maxid_pt, None, 301), 101, weights=new_data_w)
bin_centres2 = (bin_edges2[:-1] + bin_edges2[1:])*0.5
plt.scatter(bin_centres2, n2, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('pT [GeV]')
plt.ylabel('Entries')
plt.yscale('log')
plt.legend()
plt.title('Photons with MAX photonID')
plt.subplot(122)
plt.hist([np.clip(new_bkg_minid_pt[i], None, 301) for i in range(len(new_bkg_minid_pt))], 51, weights=new_bkg_w,
         stacked=True, label=['DiPhoton', 'GJet', 'Multijets'], zorder=-1)
n3, bin_edges3 = np.histogram(np.clip(new_data_minid_pt, None, 301), 101, weights=new_data_w)
bin_centres3 = (bin_edges3[:-1] + bin_edges3[1:])*0.5
plt.scatter(bin_centres3, n3, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('pT [GeV]')
plt.ylabel('Entries')
plt.yscale('log')
plt.legend()
plt.title('Photons with MIN photonID')
plt.savefig('plots/AllMCvsData_2017_pT.png')

plt.figure(3, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.subplot(121)
plt.hist(new_bkg_maxid_eta, 100, weights=new_bkg_w, stacked=True, label=['DiPhoton', 'GJet', 'Multijets'], zorder=-1)
n4, bin_edges4 = np.histogram(new_data_maxid_eta, 100, weights=new_data_w)
bin_centres4 = (bin_edges4[:-1] + bin_edges4[1:])*0.5
plt.scatter(bin_centres4, n4, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('eta')
plt.ylabel('Entries')
plt.legend()
plt.title('Photons with MAX photonID')
plt.subplot(122)
plt.hist(new_bkg_minid_eta, 100, weights=new_bkg_w, stacked=True, label=['DiPhoton', 'GJet', 'Multijets'], zorder=-1)
n5, bin_edges5 = np.histogram(new_data_minid_eta, 100, weights=new_data_w)
bin_centres5 = (bin_edges5[:-1] + bin_edges5[1:])*0.5
plt.scatter(bin_centres5, n5, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('eta')
plt.ylabel('Entries')
plt.legend()
plt.title('Photons with MIN photonID')
plt.savefig('plots/AllMCvsData_2017_eta.png')

plt.figure(4, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.hist(new_bkg_njets, 10, range=(-0.5, 9.5), weights=new_bkg_w,
         stacked=True, label=['DiPhoton', 'GJet', 'Multijets'], zorder=-1)
n6, bin_edges6 = np.histogram(new_data_njets, 10, range=(-0.5, 9.5), weights=new_data_w)
bin_centres6 = (bin_edges6[:-1] + bin_edges6[1:])/2
plt.scatter(bin_centres6, n6, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('Number of jets')
plt.ylabel('Entries')
plt.legend(loc='upper right')
plt.savefig('plots/AllMCvsData_2017_njets.png')
plt.show()
