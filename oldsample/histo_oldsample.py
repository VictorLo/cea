import os
import numpy as np
import matplotlib.pyplot as plt
import uproot as up

my_dpi = 96

lead_id = []
sublead_id = []
lead_pt = []
lead_eta = []
sublead_pt = []
sublead_eta = []
w = []

# data extraction
for dir in os.scandir("ntuples"):
    if 'GJet' in str(dir):
        for filename in os.scandir(dir):
            file = up.open(filename)
            events = file["tthHadronicTagDumper/trees/tth_13TeV_all"]
            lead_id = np.concatenate((lead_id, events.array("leadIDMVA")), axis=None)
            sublead_id = np.concatenate((sublead_id, events.array("subleadIDMVA")), axis=None)
            lead_pt = np.concatenate((lead_pt, events.array("leadPt")), axis=None)
            sublead_pt = np.concatenate((sublead_pt, events.array("subleadPt")), axis=None)
            lead_eta = np.concatenate((lead_eta, events.array("leadEta")), axis=None)
            sublead_eta = np.concatenate((sublead_eta, events.array("subleadEta")), axis=None)
            w = np.concatenate((w, events.array("weight")), axis=None)

max_id = []
min_id = []
maxid_pt = []
minid_pt = []
maxid_eta = []
minid_eta = []
for i in range(len(lead_id)):
    if lead_id[i] >= sublead_id[i]:
        max_id.append(lead_id[i])
        min_id.append(sublead_id[i])
        maxid_pt.append(lead_pt[i])
        minid_pt.append(sublead_pt[i])
        maxid_eta.append(lead_eta[i])
        minid_eta.append(sublead_eta[i])
    else:
        max_id.append(sublead_id[i])
        min_id.append(lead_id[i])
        maxid_pt.append(sublead_pt[i])
        minid_pt.append(lead_pt[i])
        maxid_eta.append(sublead_eta[i])
        minid_eta.append(lead_eta[i])

# plot histograms
plt.figure(1, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.hist(max_id, 100, weights=w, histtype='step', label='Max IDmva')
plt.hist(min_id, 100, weights=w, histtype='step', label='Min IDmva')
plt.xlabel('ID MVA')
plt.ylabel('Entries')
plt.legend(loc='upper left')
plt.savefig('plots/GJet_maxID_minID_distributions.png')

plt.figure(2, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.hist(np.clip(maxid_pt, None, 401), 101, weights=w, histtype='step', label='Max IDmva')
plt.hist(np.clip(minid_pt, None, 401), 101, weights=w, histtype='step', label='Min IDmva')
plt.xlabel('pT [GeV]')
plt.yscale('log')
plt.ylabel('Entries')
plt.legend()
plt.savefig('plots/GJet_pT_distributions.png')

plt.figure(3, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.hist(maxid_eta, 100, weights=w, histtype='step', label='Max IDmva')
plt.hist(minid_eta, 100, weights=w, histtype='step', label='Min IDmva')
plt.xlabel('eta')
plt.ylabel('Entries')
plt.legend()
plt.savefig('plots/GJet_eta_distributions.png')

plt.show()
