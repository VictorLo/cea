import numpy as np
from matplotlib import ticker
import matplotlib.pyplot as plt
import uproot as up
import pandas


def extract(dataframe, file, scalefactor=1.0, branch="tthHadronicTagDumper/trees/tth_13TeV_all", features=None):

    if features is None:
        features = ['leadIDMVA', 'subleadIDMVA', 'leadPt', 'subleadPt', 'leadEta', 'subleadEta',
                    'leadGenMatch', 'subleadGenMatch', 'n_jets', 'nvtx', 'dipho_sumpt', 'mass', 'weight']

    events = up.open(file)[branch]
    tmp_df = events.pandas.df(features)
    tmp_df['weight'] = tmp_df['weight'] * scalefactor
    tmp_df['diphoPt:mass'] = tmp_df['dipho_sumpt'] / tmp_df['mass']
    tmp_df['MaxPhotonID'] = tmp_df['leadIDMVA'].where(tmp_df['leadIDMVA'] >= tmp_df['subleadIDMVA'],
                                                      tmp_df['subleadIDMVA'])
    tmp_df['MinPhotonID'] = tmp_df['subleadIDMVA'].where(tmp_df['leadIDMVA'] >= tmp_df['subleadIDMVA'],
                                                         tmp_df['leadIDMVA'])
    tmp_df['photonPt'] = tmp_df['leadPt'].where(tmp_df['leadIDMVA'] == tmp_df['MaxPhotonID'], tmp_df['subleadPt'])
    tmp_df['photonEta'] = tmp_df['leadEta'].where(tmp_df['leadIDMVA'] == tmp_df['MaxPhotonID'], tmp_df['subleadEta'])
    tmp_df['photonPt_fake'] = tmp_df['subleadPt'].where(tmp_df['leadIDMVA'] == tmp_df['MaxPhotonID'], tmp_df['leadPt'])
    tmp_df['photonEta_fake'] = tmp_df['subleadEta'].where(tmp_df['leadIDMVA'] == tmp_df['MaxPhotonID'],
                                                          tmp_df['leadEta'])

    return dataframe.append(tmp_df, ignore_index=True)


def cuts(df):
    cutmass = '(mass > 100 and mass < 115) or (mass > 135 and mass < 180)'
    cutpt = 'leadPt > 35 and subleadPt > 25'
    cuteta = 'leadEta > -2.5 and leadEta < 2.5 and subleadEta > -2.5 and subleadEta < 2.5'
    df.query(cutmass, inplace=True)
    df.query(cutpt, inplace=True)
    df.query(cuteta, inplace=True)
    return df


def gjet_tagging(dataframes):
    df = pandas.DataFrame()
    for i in range(len(dataframes)):
        tmp_df = dataframes[i].query('(leadGenMatch != 1 and subleadGenMatch == 1) or '
                                     '(leadGenMatch == 1 and subleadGenMatch != 1)')
        df = df.append(tmp_df, ignore_index=True)
    return df


def jets_tagging(dataframes):
    df = pandas.DataFrame()
    for i in range(len(dataframes)):
        tmp_df = dataframes[i].query('leadGenMatch != 1 and subleadGenMatch != 1')
        df = df.append(tmp_df, ignore_index=True)
    return df


def dipho_tagging(dataframes):
    df = pandas.DataFrame()
    for i in range(len(dataframes)):
        tmp_df = dataframes[i].query('leadGenMatch == 1 and subleadGenMatch == 1')
        df = df.append(tmp_df, ignore_index=True)
    return df


def heatmap(data, row_labels, col_labels, ax=None, cbar_kw=None, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (N, M).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if cbar_kw is None:
        cbar_kw = {}

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=25, ha="right",
             rotation_mode="anchor")

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar


def annotate_heatmap(im, data=None, valfmt="{x:.2f}", textcolors=None, threshold=None, **textkw):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A list or array of two color specifications.  The first is used for
        values below a threshold, the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **textkw
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if textcolors is None:
        textcolors = ["black", "white"]

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[im.norm(data[i, j]) < threshold])
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts


if __name__ == '__main__':
    print('Library of useful functions !')
