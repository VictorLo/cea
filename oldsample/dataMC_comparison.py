import os
import numpy as np
import matplotlib.pyplot as plt
import uproot as up
import pandas

my_dpi = 96

features = ['leadIDMVA', 'subleadIDMVA', 'leadPt', 'subleadPt', 'leadEta', 'subleadEta', 'weight']
df_data = pandas.DataFrame(columns=features)
df_gg = pandas.DataFrame(columns=features)
df_gjet = pandas.DataFrame(columns=features)
df_jets = pandas.DataFrame(columns=features)

# data extraction
completion = 0
print("Extracting data :")
for dir in os.scandir("ntuples"):
    for filename in os.scandir(dir):
        completion += 1
        print(int((completion / 69) * 100), "%")
        events = up.open(filename)["tthHadronicTagDumper/trees/tth_13TeV_all"]
        if 'DoubleEG' in dir.name or 'EGamma' in dir.name:
            df_data = df_data.append(events.pandas.df(features), ignore_index=True)
        elif 'DiPhoton' in dir.name:
            df_gg = df_gg.append(events.pandas.df(features), ignore_index=True)
        elif 'GJet' in dir.name:
            df_gjet = df_gjet.append(events.pandas.df(features), ignore_index=True)
        elif 'QCD' in dir.name:
            df_jets = df_jets.append(events.pandas.df(features), ignore_index=True)
print("Done !")

df_data_leadmaxid = df_data[df_data['leadIDMVA'] >= df_data['subleadIDMVA']]
df_data_subleadmaxid = df_data[df_data['leadIDMVA'] < df_data['subleadIDMVA']]

data_maxid = np.concatenate((df_data_leadmaxid['leadIDMVA'].T.to_numpy(),
                             df_data_subleadmaxid['subleadIDMVA'].T.to_numpy()))
data_minid = np.concatenate((df_data_leadmaxid['subleadIDMVA'].T.to_numpy(),
                             df_data_subleadmaxid['leadIDMVA'].T.to_numpy()))
data_maxid_pt = np.concatenate((df_data_leadmaxid['leadPt'].T.to_numpy(),
                                df_data_subleadmaxid['subleadPt'].T.to_numpy()))
data_minid_pt = np.concatenate((df_data_leadmaxid['subleadPt'].T.to_numpy(),
                                df_data_subleadmaxid['leadPt'].T.to_numpy()))
data_maxid_eta = np.concatenate((df_data_leadmaxid['leadEta'].T.to_numpy(),
                                 df_data_subleadmaxid['subleadEta'].T.to_numpy()))
data_minid_eta = np.concatenate((df_data_leadmaxid['subleadEta'].T.to_numpy(),
                                 df_data_subleadmaxid['leadEta'].T.to_numpy()))
data_w = df_data['weight'].T.to_numpy()

df_gg_leadmaxid = df_gg[df_gg['leadIDMVA'] >= df_gg['subleadIDMVA']]
df_gg_subleadmaxid = df_gg[df_gg['leadIDMVA'] < df_gg['subleadIDMVA']]
df_gjet_leadmaxid = df_gjet[df_gjet['leadIDMVA'] >= df_gjet['subleadIDMVA']]
df_gjet_subleadmaxid = df_gjet[df_gjet['leadIDMVA'] < df_gjet['subleadIDMVA']]
df_jets_leadmaxid = df_jets[df_jets['leadIDMVA'] >= df_jets['subleadIDMVA']]
df_jets_subleadmaxid = df_jets[df_jets['leadIDMVA'] < df_jets['subleadIDMVA']]

bkg_maxid = [np.concatenate((df_gg_leadmaxid['leadIDMVA'].T.to_numpy(),
                             df_gg_subleadmaxid['subleadIDMVA'].T.to_numpy())),
             np.concatenate((df_gjet_leadmaxid['leadIDMVA'].T.to_numpy(),
                             df_gjet_subleadmaxid['subleadIDMVA'].T.to_numpy())),
             np.concatenate((df_jets_leadmaxid['leadIDMVA'].T.to_numpy(),
                             df_jets_subleadmaxid['subleadIDMVA'].T.to_numpy()))]
bkg_minid = [np.concatenate((df_gg_leadmaxid['subleadIDMVA'].T.to_numpy(),
                             df_gg_subleadmaxid['leadIDMVA'].T.to_numpy())),
             np.concatenate((df_gjet_leadmaxid['subleadIDMVA'].T.to_numpy(),
                             df_gjet_subleadmaxid['leadIDMVA'].T.to_numpy())),
             np.concatenate((df_jets_leadmaxid['subleadIDMVA'].T.to_numpy(),
                             df_jets_subleadmaxid['leadIDMVA'].T.to_numpy()))]
bkg_maxid_pt = [np.concatenate((df_gg_leadmaxid['leadPt'].T.to_numpy(),
                                df_gg_subleadmaxid['subleadPt'].T.to_numpy())),
                np.concatenate((df_gjet_leadmaxid['leadPt'].T.to_numpy(),
                                df_gjet_subleadmaxid['subleadPt'].T.to_numpy())),
                np.concatenate((df_jets_leadmaxid['leadPt'].T.to_numpy(),
                                df_jets_subleadmaxid['subleadPt'].T.to_numpy()))]
bkg_minid_pt = [np.concatenate((df_gg_leadmaxid['subleadPt'].T.to_numpy(),
                                df_gg_subleadmaxid['leadPt'].T.to_numpy())),
                np.concatenate((df_gjet_leadmaxid['subleadPt'].T.to_numpy(),
                                df_gjet_subleadmaxid['leadPt'].T.to_numpy())),
                np.concatenate((df_jets_leadmaxid['subleadPt'].T.to_numpy(),
                                df_jets_subleadmaxid['leadPt'].T.to_numpy()))]
bkg_maxid_eta = [np.concatenate((df_gg_leadmaxid['leadEta'].T.to_numpy(),
                                 df_gg_subleadmaxid['subleadEta'].T.to_numpy())),
                 np.concatenate((df_gjet_leadmaxid['leadEta'].T.to_numpy(),
                                 df_gjet_subleadmaxid['subleadEta'].T.to_numpy())),
                 np.concatenate((df_jets_leadmaxid['leadEta'].T.to_numpy(),
                                 df_jets_subleadmaxid['subleadEta'].T.to_numpy()))]
bkg_minid_eta = [np.concatenate((df_gg_leadmaxid['subleadEta'].T.to_numpy(),
                                 df_gg_subleadmaxid['leadEta'].T.to_numpy())),
                 np.concatenate((df_gjet_leadmaxid['subleadEta'].T.to_numpy(),
                                 df_gjet_subleadmaxid['leadEta'].T.to_numpy())),
                 np.concatenate((df_jets_leadmaxid['subleadEta'].T.to_numpy(),
                                 df_jets_subleadmaxid['leadEta'].T.to_numpy()))]
bkg_w = [df_gg['weight'].T.to_numpy(), df_gjet['weight'].T.to_numpy(), df_jets['weight'].T.to_numpy()]

# plot histograms
plt.figure(1, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.subplot(121)
plt.hist(bkg_maxid, 100, weights=bkg_w, stacked=True, label=['DiPhoton', 'GJet', 'Multijets'], zorder=-1)
n, bin_edges = np.histogram(data_maxid, 100, weights=data_w)
bin_centres = (bin_edges[:-1] + bin_edges[1:])*0.5
plt.scatter(bin_centres, n, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('ID MVA')
plt.ylabel('Entries')
plt.legend(loc='upper left')
plt.title('Photons with MAX photonID')
plt.subplot(122)
plt.hist(bkg_minid, 100, weights=bkg_w, stacked=True, label=['DiPhoton', 'GJet', 'Multijets'], zorder=-1)
n1, bin_edges1 = np.histogram(data_minid, 100, weights=data_w)
bin_centres1 = (bin_edges1[:-1] + bin_edges1[1:])*0.5
plt.scatter(bin_centres1, n1, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('ID MVA')
plt.ylabel('Entries')
plt.legend(loc='upper left')
plt.title('Photons with MIN photonID')
plt.savefig('plots/photonID_MCDataComparison.png')

plt.figure(2, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.subplot(121)
plt.hist([np.clip(bkg_maxid_pt[i], None, 401) for i in range(len(bkg_maxid_pt))], 101, weights=bkg_w, stacked=True,
         label=['DiPhoton', 'GJet', 'Multijets'], zorder=-1)
n2, bin_edges2 = np.histogram(np.clip(data_maxid_pt, None, 401), 101, weights=data_w)
bin_centres2 = (bin_edges2[:-1] + bin_edges2[1:])*0.5
plt.scatter(bin_centres2, n2, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('pT [GeV]')
plt.ylabel('Entries')
plt.yscale('log')
plt.legend()
plt.title('Photons with MAX photonID')
plt.subplot(122)
plt.hist([np.clip(bkg_minid_pt[i], None, 401) for i in range(len(bkg_minid_pt))], 101, weights=bkg_w, stacked=True,
         label=['DiPhoton', 'GJet', 'Multijets'], zorder=-1)
n3, bin_edges3 = np.histogram(np.clip(data_minid_pt, None, 401), 101, weights=data_w)
bin_centres3 = (bin_edges3[:-1] + bin_edges3[1:])*0.5
plt.scatter(bin_centres3, n3, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('pT [GeV]')
plt.ylabel('Entries')
plt.yscale('log')
plt.legend()
plt.title('Photons with MIN photonID')
plt.savefig('plots/pT_MCDataComparison.png')

plt.figure(3, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.subplot(121)
plt.hist(bkg_maxid_eta, 100, weights=bkg_w, stacked=True, label=['DiPhoton', 'GJet', 'Multijets'], zorder=-1)
n4, bin_edges4 = np.histogram(data_maxid_eta, 100, weights=data_w)
bin_centres4 = (bin_edges4[:-1] + bin_edges4[1:])*0.5
plt.scatter(bin_centres4, n4, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('eta')
plt.ylabel('Entries')
plt.legend()
plt.title('Photons with MAX photonID')
plt.subplot(122)
plt.hist(bkg_minid_eta, 100, weights=bkg_w, stacked=True, label=['DiPhoton', 'GJet', 'Multijets'], zorder=-1)
n5, bin_edges5 = np.histogram(data_minid_eta, 100, weights=data_w)
bin_centres5 = (bin_edges5[:-1] + bin_edges5[1:])*0.5
plt.scatter(bin_centres5, n5, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('eta')
plt.ylabel('Entries')
plt.legend()
plt.title('Photons with MIN photonID')
plt.savefig('plots/eta_MCDataComparison.png')

plt.show()
