import os
import numpy as np
import matplotlib.pyplot as plt
import uproot as up
import pandas

my_dpi = 96

luminosity = 41.5

features = ['leadIDMVA', 'subleadIDMVA', 'leadPt', 'subleadPt', 'leadEta', 'subleadEta', 'weight',
            'n_jets', 'nb_loose', 'mass']
df_data = pandas.DataFrame(columns=features)
df_gjet = pandas.DataFrame(columns=features)

# data extraction
completion = 0
print("Extracting data :")
for dir in os.scandir("ntuples"):
    for filename in os.scandir(dir):
        completion += 1
        print(int((completion / 69) * 100), "%")
        events = up.open(filename)["tthHadronicTagDumper/trees/tth_13TeV_all"]
        if 'DoubleEG_Run2017' in dir.name:
            print("Extracting from " + dir.name)
            df_data = df_data.append(events.pandas.df(features), ignore_index=True)
        elif 'GJet' in dir.name and 'Fall17' in dir.name and 'HT' not in dir.name:
            print("Extracting from " + dir.name)
            tmp_df = events.pandas.df(features)
            if '20to40' in dir.name:
                scale1fb = 0.013174044112514596
                tmp_df['weight'] = tmp_df['weight'] * scale1fb * luminosity
                df_gjet = df_gjet.append(tmp_df, ignore_index=True)
            if '20toInf' in dir.name:
                scale1fb = 0.075237801398417098
                tmp_df['weight'] = tmp_df['weight'] * scale1fb * luminosity
                df_gjet = df_gjet.append(tmp_df, ignore_index=True)
            if '40toInf' in dir.name:
                scale1fb = 0.011081055034051624
                tmp_df['weight'] = tmp_df['weight'] * scale1fb * luminosity
                df_gjet = df_gjet.append(tmp_df, ignore_index=True)
print("Done !")

# sort the photons by photonID value not by pT
df_data_leadmaxid = df_data[(df_data['leadIDMVA'] >= df_data['subleadIDMVA']) & (df_data['n_jets'] > 1) &
                            (df_data['nb_loose'] > 0) & (((df_data['mass'] > 100) & (df_data['mass'] < 115)) |
                                                         ((df_data['mass'] > 135) & (df_data['mass'] < 180)))]
df_data_subleadmaxid = df_data[(df_data['leadIDMVA'] < df_data['subleadIDMVA']) & (df_data['n_jets'] > 1) &
                               (df_data['nb_loose'] > 0) & (((df_data['mass'] > 100) & (df_data['mass'] < 115)) |
                                                            ((df_data['mass'] > 135) & (df_data['mass'] < 180)))]

data_maxid = np.concatenate((df_data_leadmaxid['leadIDMVA'].T.to_numpy(),
                             df_data_subleadmaxid['subleadIDMVA'].T.to_numpy()))
data_minid = np.concatenate((df_data_leadmaxid['subleadIDMVA'].T.to_numpy(),
                             df_data_subleadmaxid['leadIDMVA'].T.to_numpy()))
data_maxid_pt = np.concatenate((df_data_leadmaxid['leadPt'].T.to_numpy(),
                                df_data_subleadmaxid['subleadPt'].T.to_numpy()))
data_minid_pt = np.concatenate((df_data_leadmaxid['subleadPt'].T.to_numpy(),
                                df_data_subleadmaxid['leadPt'].T.to_numpy()))
data_maxid_eta = np.concatenate((df_data_leadmaxid['leadEta'].T.to_numpy(),
                                 df_data_subleadmaxid['subleadEta'].T.to_numpy()))
data_minid_eta = np.concatenate((df_data_leadmaxid['subleadEta'].T.to_numpy(),
                                 df_data_subleadmaxid['leadEta'].T.to_numpy()))
data_w = np.concatenate((df_data_leadmaxid['weight'].T.to_numpy(),
                         df_data_subleadmaxid['weight'].T.to_numpy()))

df_gjet_leadmaxid = df_gjet[(df_gjet['leadIDMVA'] >= df_gjet['subleadIDMVA']) & (df_gjet['n_jets'] > 1) &
                            (df_gjet['nb_loose'] > 0) & (((df_gjet['mass'] > 100) & (df_gjet['mass'] < 115)) |
                                                         ((df_gjet['mass'] > 135) & (df_gjet['mass'] < 180)))]
df_gjet_subleadmaxid = df_gjet[(df_gjet['leadIDMVA'] < df_gjet['subleadIDMVA']) & (df_gjet['n_jets'] > 1) &
                               (df_gjet['nb_loose'] > 0) & (((df_gjet['mass'] > 100) & (df_gjet['mass'] < 115)) |
                                                            ((df_gjet['mass'] > 135) & (df_gjet['mass'] < 180)))]

bkg_maxid = np.concatenate((df_gjet_leadmaxid['leadIDMVA'].T.to_numpy(),
                            df_gjet_subleadmaxid['subleadIDMVA'].T.to_numpy()))
bkg_minid = np.concatenate((df_gjet_leadmaxid['subleadIDMVA'].T.to_numpy(),
                            df_gjet_subleadmaxid['leadIDMVA'].T.to_numpy()))
bkg_maxid_pt = np.concatenate((df_gjet_leadmaxid['leadPt'].T.to_numpy(),
                               df_gjet_subleadmaxid['subleadPt'].T.to_numpy()))
bkg_minid_pt = np.concatenate((df_gjet_leadmaxid['subleadPt'].T.to_numpy(),
                               df_gjet_subleadmaxid['leadPt'].T.to_numpy()))
bkg_maxid_eta = np.concatenate((df_gjet_leadmaxid['leadEta'].T.to_numpy(),
                                df_gjet_subleadmaxid['subleadEta'].T.to_numpy()))
bkg_minid_eta = np.concatenate((df_gjet_leadmaxid['subleadEta'].T.to_numpy(),
                                df_gjet_subleadmaxid['leadEta'].T.to_numpy()))
bkg_w = np.concatenate((df_gjet_leadmaxid['weight'].T.to_numpy(),
                        df_gjet_subleadmaxid['weight'].T.to_numpy()))

# select only data with g+jet behaviour
select_data_maxid = data_maxid >= 0.5
select_data_minid = data_minid <= -0.2

new_data_maxid = data_maxid[select_data_maxid & select_data_minid]
new_data_minid = data_minid[select_data_maxid & select_data_minid]
new_data_maxid_pt = data_maxid_pt[select_data_maxid & select_data_minid]
new_data_minid_pt = data_minid_pt[select_data_maxid & select_data_minid]
new_data_maxid_eta = data_maxid_eta[select_data_maxid & select_data_minid]
new_data_minid_eta = data_minid_eta[select_data_maxid & select_data_minid]
new_data_w = data_w[select_data_maxid & select_data_minid]

select_bkg_maxid = bkg_maxid >= 0.5
select_bkg_minid = bkg_minid <= -0.2

new_bkg_maxid = bkg_maxid[select_bkg_maxid & select_bkg_minid]
new_bkg_minid = bkg_minid[select_bkg_maxid & select_bkg_minid]
new_bkg_maxid_pt = bkg_maxid_pt[select_bkg_maxid & select_bkg_minid]
new_bkg_minid_pt = bkg_minid_pt[select_bkg_maxid & select_bkg_minid]
new_bkg_maxid_eta = bkg_maxid_eta[select_bkg_maxid & select_bkg_minid]
new_bkg_minid_eta = bkg_minid_eta[select_bkg_maxid & select_bkg_minid]
new_bkg_w = bkg_w[select_bkg_maxid & select_bkg_minid]

# plot histograms
plt.figure(1, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.subplot(121)
plt.hist(new_bkg_maxid, 100, weights=new_bkg_w, label='GJet', zorder=-1)
n, bin_edges = np.histogram(new_data_maxid, 100, weights=new_data_w)
bin_centres = (bin_edges[:-1] + bin_edges[1:])*0.5
plt.scatter(bin_centres, n, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('ID MVA')
plt.ylabel('Entries')
plt.legend(loc='upper left')
plt.title('Photons with MAX photonID')
plt.subplot(122)
plt.hist(new_bkg_minid, 100, weights=new_bkg_w, label='GJet', zorder=-1)
n1, bin_edges1 = np.histogram(new_data_minid, 100, weights=new_data_w)
bin_centres1 = (bin_edges1[:-1] + bin_edges1[1:])*0.5
plt.scatter(bin_centres1, n1, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('ID MVA')
plt.ylabel('Entries')
plt.legend(loc='upper left')
plt.title('Photons with MIN photonID')
#plt.savefig('plots/GJetMCvsData_2017_photonID.png')

plt.figure(2, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.subplot(121)
plt.hist(np.clip(new_bkg_maxid_pt, None, 401), 101, weights=new_bkg_w, label='GJet', zorder=-1)
n2, bin_edges2 = np.histogram(np.clip(new_data_maxid_pt, None, 401), 101, weights=new_data_w)
bin_centres2 = (bin_edges2[:-1] + bin_edges2[1:])*0.5
plt.scatter(bin_centres2, n2, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('pT [GeV]')
plt.ylabel('Entries')
plt.yscale('log')
plt.legend()
plt.title('Photons with MAX photonID')
plt.subplot(122)
plt.hist(np.clip(new_bkg_minid_pt, None, 401), 101, weights=new_bkg_w, label='GJet', zorder=-1)
n3, bin_edges3 = np.histogram(np.clip(new_data_minid_pt, None, 401), 101, weights=new_data_w)
bin_centres3 = (bin_edges3[:-1] + bin_edges3[1:])*0.5
plt.scatter(bin_centres3, n3, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('pT [GeV]')
plt.ylabel('Entries')
plt.yscale('log')
plt.legend()
plt.title('Photons with MIN photonID')
#plt.savefig('plots/GJetMCvsData_2017_pT.png')

plt.figure(3, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.subplot(121)
plt.hist(new_bkg_maxid_eta, 100, weights=new_bkg_w, label='GJet', zorder=-1)
n4, bin_edges4 = np.histogram(new_data_maxid_eta, 100, weights=new_data_w)
bin_centres4 = (bin_edges4[:-1] + bin_edges4[1:])*0.5
plt.scatter(bin_centres4, n4, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('eta')
plt.ylabel('Entries')
plt.legend()
plt.title('Photons with MAX photonID')
plt.subplot(122)
plt.hist(new_bkg_minid_eta, 100, weights=new_bkg_w, label='GJet', zorder=-1)
n5, bin_edges5 = np.histogram(new_data_minid_eta, 100, weights=new_data_w)
bin_centres5 = (bin_edges5[:-1] + bin_edges5[1:])*0.5
plt.scatter(bin_centres5, n5, marker='o', color='black', label='Data', zorder=1)
plt.xlabel('eta')
plt.ylabel('Entries')
plt.legend()
plt.title('Photons with MIN photonID')
#plt.savefig('plots/GJetMCvsData_2017_eta.png')

plt.show()
