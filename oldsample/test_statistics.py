import os
import numpy as np
import matplotlib.pyplot as plt
import uproot as up
import pandas
import lib

my_dpi = 96

""" Global variables """
luminosity = 41.5
branchpath = "tthHadronicTagDumper/trees/tth_13TeV_all"
observables = ['leadIDMVA', 'subleadIDMVA', 'leadPt', 'subleadPt', 'leadEta', 'subleadEta',
               'leadGenMatch', 'subleadGenMatch', 'n_jets', 'nb_loose', 'mass', 'weight']

""" Main """
df_pythia_gjet = pandas.DataFrame()
df_pythia_jets = pandas.DataFrame()
df_madgraph_gjet = pandas.DataFrame()

# data extraction
completion = 0
print("Extracting data :")
for folder in os.scandir("ntuples"):
    for filename in os.scandir(folder):
        completion += 1
        print(int((completion / 69) * 100), "%")
        if 'GJets_' in folder.name:
            if 'Fall17' in folder.name:
                if '40To100' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 3.346080992393051
                    df_madgraph_gjet = lib.extract(df_madgraph_gjet, filename, scale1fb * luminosity)
                elif '100To200' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.86681778146470212
                    df_madgraph_gjet = lib.extract(df_madgraph_gjet, filename, scale1fb * luminosity)
                elif '200To400' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.11795314318135013
                    df_madgraph_gjet = lib.extract(df_madgraph_gjet, filename, scale1fb * luminosity)
                elif '400To600' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.055537261041074731
                    df_madgraph_gjet = lib.extract(df_madgraph_gjet, filename, scale1fb * luminosity)
                elif '600ToInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.022394866068735982
                    df_madgraph_gjet = lib.extract(df_madgraph_gjet, filename, scale1fb * luminosity)
            elif 'Autumn18' in folder.name:
                if '40To100' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 1.9893071004836207
                    df_madgraph_gjet = lib.extract(df_madgraph_gjet, filename, scale1fb * luminosity)
                elif '100To200' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.88113062363417383
                    df_madgraph_gjet = lib.extract(df_madgraph_gjet, filename, scale1fb * luminosity)
                elif '200To400' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.11468868362468057
                    df_madgraph_gjet = lib.extract(df_madgraph_gjet, filename, scale1fb * luminosity)
                elif '400To600' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.055407883724490792
                    df_madgraph_gjet = lib.extract(df_madgraph_gjet, filename, scale1fb * luminosity)
                elif '600ToInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.017182858089356656
                    df_madgraph_gjet = lib.extract(df_madgraph_gjet, filename, scale1fb * luminosity)
            elif 'Summer16' in folder.name:
                if '40To100' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 2.476909254730173
                    df_madgraph_gjet = lib.extract(df_madgraph_gjet, filename, scale1fb * luminosity)
                elif '100To200' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.90160928845608568
                    df_madgraph_gjet = lib.extract(df_madgraph_gjet, filename, scale1fb * luminosity)
                elif '200To400' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.11107048269770327
                    df_madgraph_gjet = lib.extract(df_madgraph_gjet, filename, scale1fb * luminosity)
                elif '400To600' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.053951822800870346
                    df_madgraph_gjet = lib.extract(df_madgraph_gjet, filename, scale1fb * luminosity)
                elif '600ToInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.018599224500906047
                    df_madgraph_gjet = lib.extract(df_madgraph_gjet, filename, scale1fb * luminosity)
        elif 'GJet_' in folder.name:
            if 'Fall17' in folder.name:
                if '20to40' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.013174044112514596
                    df_pythia_gjet = lib.extract(df_pythia_gjet, filename, scale1fb * luminosity)
                elif '20toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.075237801398417098
                    df_pythia_gjet = lib.extract(df_pythia_gjet, filename, scale1fb * luminosity)
                elif '40toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.011081055034051624
                    df_pythia_gjet = lib.extract(df_pythia_gjet, filename, scale1fb * luminosity)
            if 'Summer16' in folder.name:
                if '20to40' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.0087894013882300163
                    df_pythia_gjet = lib.extract(df_pythia_gjet, filename, scale1fb * luminosity)
                elif '20toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.085869509958513487
                    df_pythia_gjet = lib.extract(df_pythia_gjet, filename, scale1fb * luminosity)
                elif '40toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.011694009018159155
                    df_pythia_gjet = lib.extract(df_pythia_gjet, filename, scale1fb * luminosity)
        elif 'QCD' in folder.name:
            if 'Fall17' in folder.name:
                if '30to40' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 1.6912594606743319
                    df_pythia_jets = lib.extract(df_pythia_jets, filename, scale1fb * luminosity)
                if '30toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 5.7410435024360789
                    df_pythia_jets = lib.extract(df_pythia_jets, filename, scale1fb * luminosity)
                if '40toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 5.7268841667121686
                    df_pythia_jets = lib.extract(df_pythia_jets, filename, scale1fb * luminosity)
            if 'Summer16' in folder.name:
                if '30to40' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 1.2276976060675346
                    df_pythia_jets = lib.extract(df_pythia_jets, filename, scale1fb * luminosity)
                if '30toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 6.9258319173707044
                    df_pythia_jets = lib.extract(df_pythia_jets, filename, scale1fb * luminosity)
                if '40toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 5.447683312621888
                    df_pythia_jets = lib.extract(df_pythia_jets, filename, scale1fb * luminosity)
print("Done !")
print(len(df_pythia_gjet.index) + len(df_pythia_jets.index))
# apply cuts
lib.cuts(df_pythia_gjet)
lib.cuts(df_pythia_jets)
lib.cuts(df_madgraph_gjet)
print(len(df_pythia_gjet.index) + len(df_pythia_jets.index))

# same selection as for the gan
selection = 'MaxPhotonID > -0.2 and MinPhotonID > -0.2'
df_pythia_gjet.query(selection, inplace=True)
df_pythia_jets.query(selection, inplace=True)
df_madgraph_gjet.query(selection, inplace=True)
print(len(df_pythia_gjet.index) + len(df_pythia_jets.index))

# tagging of events
df_pythia_gjet_final = lib.gjet_tagging([df_pythia_gjet, df_pythia_jets])
df_pythia_jets_final = lib.jets_tagging([df_pythia_gjet, df_pythia_jets])
df_pythia_dipho_final = lib.dipho_tagging([df_pythia_gjet, df_pythia_jets])
df_madgraph_gjet_final = lib.gjet_tagging([df_madgraph_gjet])

print(f"GJet : {len(df_pythia_gjet_final.index)}")
print(f"Jets : {len(df_pythia_jets_final.index)}")
print(f"Dipho : {len(df_pythia_dipho_final.index)}")
