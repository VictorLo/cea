import os
import numpy as np
import matplotlib.pyplot as plt
import uproot as up
import pandas

my_dpi = 96

""" Global variables """
luminosity = 41.5
branchpath = "tthHadronicTagDumper/trees/tth_13TeV_all"
observables = ['leadIDMVA', 'subleadIDMVA', 'leadPt', 'subleadPt', 'leadEta', 'subleadEta',
               'leadGenMatch', 'subleadGenMatch', 'n_jets', 'nb_loose', 'mass', 'weight']

""" Functions """


def extract(dataframe, file, scalefactor=1.0, branch=branchpath, features=None):

    if features is None:
        features = observables

    events = up.open(file)[branch]
    tmp_df = events.pandas.df(features)
    tmp_df['weight'] = tmp_df['weight'] * scalefactor
    tmp_df['MaxPhotonID'] = tmp_df['leadIDMVA'].where(tmp_df['leadIDMVA'] >= tmp_df['subleadIDMVA'],
                                                      tmp_df['subleadIDMVA'])
    tmp_df['MinPhotonID'] = tmp_df['subleadIDMVA'].where(tmp_df['leadIDMVA'] >= tmp_df['subleadIDMVA'],
                                                         tmp_df['leadIDMVA'])
    tmp_df['photonPt'] = tmp_df['leadPt'].where(tmp_df['leadIDMVA'] == tmp_df['MaxPhotonID'], tmp_df['subleadPt'])
    tmp_df['photonEta'] = tmp_df['leadEta'].where(tmp_df['leadIDMVA'] == tmp_df['MaxPhotonID'], tmp_df['subleadEta'])
    tmp_df['photonPt_fake'] = tmp_df['subleadPt'].where(tmp_df['leadIDMVA'] == tmp_df['MaxPhotonID'], tmp_df['leadPt'])
    tmp_df['photonEta_fake'] = tmp_df['subleadEta'].where(tmp_df['leadIDMVA'] == tmp_df['MaxPhotonID'],
                                                          tmp_df['leadEta'])

    return dataframe.append(tmp_df, ignore_index=True)


def cuts(df):
    cutmass = '(mass > 100 and mass < 115) or (mass > 135 and mass < 180)'
    cutpt = 'leadPt > 35 and subleadPt > 25'
    cuteta = 'leadEta > -2.5 and leadEta < 2.5 and subleadEta > -2.5 and subleadEta < 2.5'
    # cutnjets = 'n_jets > 1'
    # cutnbjets = 'nb_loose > 0'
    df.query(cutmass, inplace=True)
    df.query(cutpt, inplace=True)
    df.query(cuteta, inplace=True)
    # df.query(cutnjets, inplace=True)
    # df.query(cutnbjets, inplace=True)
    return df


def gjet_tagging(dataframes):
    df = dataframes[0].query('(leadGenMatch != 1 and subleadGenMatch == 1) or '
                             '(leadGenMatch == 1 and subleadGenMatch != 1)')
    for i in range(1, len(dataframes)):
        tmp_df = dataframes[i].query('(leadGenMatch != 1 and subleadGenMatch == 1) or '
                                     '(leadGenMatch == 1 and subleadGenMatch != 1)')
        df = df.append(tmp_df, ignore_index=True)
    return df


def jets_tagging(dataframes):
    df = dataframes[0].query('leadGenMatch != 1 and subleadGenMatch != 1')
    for i in range(1, len(dataframes)):
        tmp_df = dataframes[i].query('leadGenMatch != 1 and subleadGenMatch != 1')
        df = df.append(tmp_df, ignore_index=True)
    return df


""" Main """
df_gjet = pandas.DataFrame()
df_jets = pandas.DataFrame()

# data extraction
completion = 0
print("Extracting data :")
for folder in os.scandir("ntuples"):
    for filename in os.scandir(folder):
        completion += 1
        print(int((completion / 69) * 100), "%")
        if 'GJets_' in folder.name and 'Fall17' in folder.name:
            if '40To100' in folder.name:
                print("Extracting from " + folder.name)
                scale1fb = 3.346080992393051
                df_gjet = extract(df_gjet, filename, scale1fb * luminosity)
            elif '100To200' in folder.name:
                print("Extracting from " + folder.name)
                scale1fb = 0.86681778146470212
                df_gjet = extract(df_gjet, filename, scale1fb * luminosity)
            elif '200To400' in folder.name:
                print("Extracting from " + folder.name)
                scale1fb = 0.11795314318135013
                df_gjet = extract(df_gjet, filename, scale1fb * luminosity)
            elif '400To600' in folder.name:
                print("Extracting from " + folder.name)
                scale1fb = 0.055537261041074731
                df_gjet = extract(df_gjet, filename, scale1fb * luminosity)
            elif '600ToInf' in folder.name:
                print("Extracting from " + folder.name)
                scale1fb = 0.022394866068735982
                df_gjet = extract(df_gjet, filename, scale1fb * luminosity)
        elif 'QCD' in folder.name and 'Fall17' in folder.name:
            if '30to40' in folder.name:
                print("Extracting from " + folder.name)
                scale1fb = 1.6912594606743319
                df_jets = extract(df_jets, filename, scale1fb * luminosity)
            if '30toInf' in folder.name:
                print("Extracting from " + folder.name)
                scale1fb = 5.7410435024360789
                df_jets = extract(df_jets, filename, scale1fb * luminosity)
            if '40toInf' in folder.name:
                print("Extracting from " + folder.name)
                scale1fb = 5.7268841667121686
                df_jets = extract(df_jets, filename, scale1fb * luminosity)
print("Done !")

# apply cuts
cuts(df_gjet)

# select photonID region
select_photonid1 = 'MaxPhotonID >= 0.9 and MinPhotonID <= 0.5'
select_photonid2 = 'MaxPhotonID >= 0.9 and MinPhotonID >= 0.5'

df_gjet_final1 = df_gjet.query(select_photonid1)
df_gjet_final2 = df_gjet.query(select_photonid2)

# scaling
s1 = df_gjet_final1['weight'].sum()
s2 = df_gjet_final2['weight'].sum()
scale = s2/s1

# plot histograms
savepath = 'plots/GJet_photonIDregions/Method2/'

plt.figure(1, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.hist(np.clip(df_gjet_final1['photonPt'].T.to_numpy(), None, 201), 51,
         weights=df_gjet_final1['weight'].T.to_numpy() * scale, histtype='step', label='MinPhotonID <= 0.5')
plt.hist(np.clip(df_gjet_final2['photonPt'].T.to_numpy(), None, 201), 51, weights=df_gjet_final2['weight'].T.to_numpy(),
         histtype='step', label='MinPhotonID >= 0.5')
plt.xlabel('pT [GeV]')
plt.ylabel('Entries')
plt.yscale('log')
plt.title('GJet tagged events : "true" photon')
plt.legend()
plt.savefig(savepath + 'GJet_2_2017_pT_truePhoton.png')

plt.figure(2, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.hist(np.clip(df_gjet_final1['photonPt_fake'].T.to_numpy(), None, 201), 51,
         weights=df_gjet_final1['weight'].T.to_numpy() * scale, histtype='step', label='MinPhotonID <= 0.5')
plt.hist(np.clip(df_gjet_final2['photonPt_fake'].T.to_numpy(), None, 201), 51,
         weights=df_gjet_final2['weight'].T.to_numpy(), histtype='step', label='MinPhotonID >= 0.5')
plt.xlabel('pT [GeV]')
plt.ylabel('Entries')
plt.yscale('log')
plt.title('GJet tagged events : "fake" photon')
plt.legend()
plt.savefig(savepath + 'GJet_2_2017_pT_fakePhoton.png')

plt.figure(3, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)

plt.hist(df_gjet_final1['photonEta'].T.to_numpy(), 20, weights=df_gjet_final1['weight'].T.to_numpy() * scale,
         histtype='step', label='MinPhotonID <= 0.5')
plt.hist(df_gjet_final2['photonEta'].T.to_numpy(), 20, weights=df_gjet_final2['weight'].T.to_numpy(),
         histtype='step', label='MinPhotonID >= 0.5')
plt.xlabel('eta')
plt.ylabel('Entries')
plt.legend()
plt.title('GJet tagged events : "true" photon')
plt.savefig(savepath + 'GJet_2_2017_eta_truePhoton.png')

plt.figure(4, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.hist(df_gjet_final1['photonEta_fake'].T.to_numpy(), 20, weights=df_gjet_final1['weight'].T.to_numpy() * scale,
         histtype='step', label='MinPhotonID <= 0.5')
plt.hist(df_gjet_final2['photonEta_fake'].T.to_numpy(), 20, weights=df_gjet_final2['weight'].T.to_numpy(),
         histtype='step', label='MinPhotonID >= 0.5')
plt.xlabel('eta')
plt.ylabel('Entries')
plt.legend()
plt.title('GJet tagged events : "fake" photon')
plt.savefig(savepath + 'GJet_2_2017_eta_fakePhoton.png')

plt.show()
