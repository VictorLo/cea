from __future__ import print_function, division
import os
from time import time
import numpy as np
import matplotlib.pyplot as plt
import pandas
from keras.models import Sequential, Model, save_model
from keras.layers import Dense, Input, concatenate, Conv2D, Conv2DTranspose, Reshape, Flatten
from keras.layers.advanced_activations import LeakyReLU
from tensorflow.keras.optimizers import Adam
import toolsGAN as tlg


class GAN:
    def __init__(self):
        np.random.seed(int(time()))

        self.features = ['MinPhotonID', 'photonPt_fake', 'photonEta_fake', 'n_jets', 'nvtx', 'diphoPt:mass', 'weight']
        self.feat_dim = len(self.features)
        self.latent_dim = self.feat_dim - 1

        self.model_savepath = 'models/GJets_pythia_1617/Model_customloss/'
        self.plot_savepath = 'plots/GJets_pythia_1617/Model_customloss/'
        self.genModelName = "genModel"
        self.discModelName = "discModel"
        self.ganModelName = "ganModel"

        self.optimizer = Adam(epsilon=0.1)

        # here is a nice place define combined model
        self.generator = self.generatorbuild()
        self.discriminator = self.discriminatorbuild()
        self.discriminator.trainable = False

        noise = Input(shape=(self.latent_dim,))
        photon_variables = Input(shape=(self.feat_dim - 1,))
        gen_event = self.generator([noise, photon_variables])
        gan_output = self.discriminator(gen_event)

        self.gan = Model(inputs=[noise, photon_variables], outputs=gan_output)

    def generatorbuild(self):
        generator = Sequential()

        generator.add(Dense(256, input_dim=2 * self.latent_dim))
        generator.add(LeakyReLU(0.2))

        generator.add(Reshape((8, 8, 4)))

        generator.add(Conv2D(64, 2, padding='same'))
        generator.add(LeakyReLU(0.2))

        generator.add(Conv2DTranspose(32, 2, padding='same'))
        generator.add(LeakyReLU(0.2))

        generator.add(Conv2DTranspose(16, 3, padding='same'))
        generator.add(LeakyReLU(0.2))

        generator.add(Flatten())
        generator.add(Dense(1, activation='tanh'))
        generator.summary()

        photon_variables = Input(shape=(self.feat_dim - 1,))
        noise = Input(shape=(self.latent_dim,))
        gen_input = concatenate([noise, photon_variables], axis=1)
        gen_photonid = generator(gen_input)
        gen_event = concatenate([gen_photonid, photon_variables], axis=1)

        model = Model(inputs=[noise, photon_variables], outputs=gen_event)
        return model

    def discriminatorbuild(self):
        discriminator = Sequential()

        discriminator.add(Dense(256, input_dim=self.feat_dim))
        discriminator.add(LeakyReLU(0.2))

        discriminator.add(Reshape((8, 8, 4)))

        discriminator.add(Conv2D(64, 2, padding='same'))
        discriminator.add(LeakyReLU(0.2))

        discriminator.add(Conv2DTranspose(32, 2, padding='same'))
        discriminator.add(LeakyReLU(0.2))

        discriminator.add(Conv2DTranspose(16, 3, padding='same'))
        discriminator.add(LeakyReLU(0.2))

        discriminator.add(Flatten())
        discriminator.add(Dense(1, activation='sigmoid'))

        discriminator.summary()
        return discriminator

    def train(self, training_df, epochs, batch_size=256, steps=100, saveloss=True):
        # Generate samples
        df = training_df[self.features]
        df_weight = df['weight']
        df_minmax = self.get_minmax(df)
        df_norm = (df - (df.max() + df.min()) / 2) * 2 / (df.max() - df.min())

        # training sample
        df_train = df_norm.sample(frac=0.75, random_state=0)
        x_train = df_train.to_numpy()

        # test sample
        # df_test_norm = df_norm.drop(df_train.index)
        # df_weight_test = df_weight.drop(df_train.index)
        # df_test = df_test_norm * ((df_minmax.loc['max'] - df_minmax.loc['min']) / 2) + (df_minmax.loc['max'] +
        #                                                                                df_minmax.loc['min']) / 2
        # x_test = df_test.to_numpy()
        # x_weight = df_weight_test.to_numpy()
        x_test = df.to_numpy()
        x_weight = df_weight.to_numpy()

        # store values for a plot of the loss functions
        epoch_values = []
        dloss_values = []
        gloss_values = []

        # training
        for epoch in range(epochs):
            plot_dloss = 0
            plot_gloss = 0
            for batch in range(steps):

                fake_x = x_train[np.random.randint(0, x_train.shape[0], size=int(batch_size / 2))]
                photon_variables = fake_x[:, 1:]
                noise = np.random.normal(0, 1, size=(int(batch_size / 2), self.latent_dim))
                fake_events = self.generator.predict([noise, photon_variables])

                real_events = x_train[np.random.randint(0, x_train.shape[0], size=int(batch_size / 2))]

                events = np.concatenate((real_events, fake_events))
                idreal = np.concatenate((real_events[:, 0], fake_x[:, 0]))
                idgen = np.concatenate((real_events[:, 0], fake_events[:, 0]))
                pt = np.concatenate((real_events[:, 1], fake_events[:, 1]))

                disc_y = np.concatenate((np.ones(int(batch_size / 2)), np.zeros(int(batch_size / 2))))
                disc_y[int(batch_size / 2):] -= 0.05 * np.random.randint(0, 2, int(batch_size / 2))

                # Train the discriminator
                self.discriminator.compile(loss=tlg.loss_func(idreal, idgen, pt), optimizer=self.optimizer)
                d_loss = self.discriminator.train_on_batch(events, disc_y)

                # Train the generator
                fake_x2 = x_train[np.random.randint(0, x_train.shape[0], size=batch_size)]
                photon_variables2 = fake_x2[:, 1:]
                noise2 = np.random.normal(0, 1, size=(batch_size, self.latent_dim))
                fake_events2 = self.generator.predict([noise2, photon_variables2])
                y_gen = np.ones(batch_size)

                idreal2 = fake_x2[:, 0]
                idgen2 = fake_events2[:, 0]
                pt2 = fake_events2[:, 1]
                self.gan.compile(loss=tlg.loss_func(idreal2, idgen2, pt2), optimizer=self.optimizer)
                g_loss = self.gan.train_on_batch([noise2, photon_variables2], y_gen)

                if batch == steps-1:
                    print(f'Epoch: {epoch} \t Discriminator Loss: {d_loss} \t\t Generator Loss: {g_loss}')
                    plot_dloss = d_loss
                    plot_gloss = g_loss

            epoch_values.append(epoch)
            dloss_values.append(plot_dloss)
            gloss_values.append(plot_gloss)

        # create generated sample
        # df_gen_norm = df_test_norm.drop(columns=['MinPhotonID'])
        df_gen_norm = df_norm.drop(columns=['MinPhotonID'])
        noise_test = np.random.normal(0, 1, size=(len(df_gen_norm.index), self.latent_dim))
        gen_events = self.generator.predict([noise_test, df_gen_norm.to_numpy()])
        df_gen_norm['MinPhotonID'] = gen_events[:, 0]
        df_gen_norm = df_gen_norm[self.features]
        df_gen = df_gen_norm * ((df_minmax.loc['max'] - df_minmax.loc['min']) / 2) + (df_minmax.loc['max'] +
                                                                                      df_minmax.loc['min']) / 2
        x_gen = df_gen.to_numpy()

        # plot correlation matrix
        # real_correlations = df_test_norm.corr('kendall')
        real_correlations = df_norm.corr('kendall')
        gen_correlations = df_gen_norm.corr('kendall')

        plt.figure()
        plt.hist(x_test[:, 0], 20, weights=x_weight, histtype='step', label='Real')
        plt.hist(x_gen[:, 0], 20, weights=x_weight, histtype='step', label='Generated')
        plt.xlabel('PhotonID')
        plt.ylabel('Entries')
        plt.legend(loc='upper right')
        plt.savefig("{:s}1D_photonID.png".format(self.plot_savepath))
        plt.close()

        plt.figure(figsize=(1200 / 96, 720 / 96), dpi=96)
        im, _ = tlg.heatmap(real_correlations, self.features, self.features, origin='lower')
        tlg.annotate_heatmap(im)
        plt.savefig(self.plot_savepath + "corrReal.png")
        plt.close()

        self.plotfeatures2d(x_test, 'real', x_weight)

        plt.figure(figsize=(1200 / 96, 720 / 96), dpi=96)
        im, _ = tlg.heatmap(gen_correlations, self.features, self.features, origin='lower')
        tlg.annotate_heatmap(im)
        plt.savefig(self.plot_savepath + "corrGen.png")
        plt.close()

        self.plotfeatures2d(x_gen, 'gen', x_weight)

        if saveloss:
            plt.figure()
            plt.plot(epoch_values, dloss_values, label='Discriminator')
            plt.plot(epoch_values, gloss_values, label='Generator')
            plt.title('Loss functions evolution')
            plt.xlabel('Epoch')
            plt.ylabel('Loss')
            plt.legend(loc='upper right')
            plt.savefig(self.plot_savepath + "loss_evolution.png")
            plt.close()

    # def evaluate(self):

    def get_minmax(self, df):
        df_minmax = pandas.DataFrame(index=['min', 'max'], columns=self.features)
        df_minmax.loc['min'] = df.min()
        df_minmax.loc['max'] = df.max()
        return df_minmax

    def plotfeatures2d(self, arrsamp, title, df_weight):
        bins = [10, 10, 10, 6, 10, 10, 10]
        ranges = [[-0.2, 1], [25, 125], None, [1.5, 7.5], [1, 80], [0, 2], [0, 2]]
        for i in range(1, len(self.features)):
            tlg.plot2d_meanproj(arrsamp[:, 0], arrsamp[:, i], self.features[i], df_weight,
                                bins=(bins[0], bins[i]), range=[ranges[0], ranges[i]])
            plt.savefig("{:s}2D{:s}_{:d}.png".format(self.plot_savepath, title, i))
            plt.close()

    def savemodels(self):
        save_model(self.generator, self.model_savepath + self.genModelName)
        save_model(self.discriminator, self.model_savepath + self.discModelName)
        save_model(self.gan, self.model_savepath + self.ganModelName)


if __name__ == '__main__':
    lumi16 = 35.9
    lumi17 = 41.5

    df_gjet = pandas.DataFrame()
    completion = 0
    print("Extracting data :")
    for folder in os.scandir("../ntuples"):
        for filename in os.scandir(folder):
            completion += 1
            print(int((completion / 69) * 100), "%")
            if 'GJet_' in folder.name:
                if 'Fall17' in folder.name:
                    if '20to40' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.013174044112514596
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi17)
                    elif '20toInf' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.075237801398417098
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi17)
                    elif '40toInf' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.011081055034051624
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi17)
                if 'Summer16' in folder.name:
                    if '20to40' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.0087894013882300163
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi16)
                    elif '20toInf' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.085869509958513487
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi16)
                    elif '40toInf' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.011694009018159155
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi16)
    print("Done !")

    tlg.cuts(df_gjet)
    df_gjet_tagged = tlg.gjet_tagging([df_gjet])
    df_gjet_tagged.query('MaxPhotonID > -0.2 and MinPhotonID > -0.2', inplace=True)

    gan = GAN()
    gan.train(df_gjet_tagged, epochs=20, batch_size=50, steps=500)
    gan.savemodels()
