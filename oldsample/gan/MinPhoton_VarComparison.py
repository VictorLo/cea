import os
import numpy as np
import matplotlib.pyplot as plt
import pandas
import toolsGAN as tlg

lumi16 = 35.9
lumi17 = 41.5

df_gjet = pandas.DataFrame()
completion = 0
print("Extracting data :")
for folder in os.scandir("../ntuples"):
    for filename in os.scandir(folder):
        completion += 1
        print(int((completion / 69) * 100), "%")
        if 'GJet_' in folder.name:
            if 'Fall17' in folder.name:
                if '20to40' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.013174044112514596
                    df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi17)
                elif '20toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.075237801398417098
                    df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi17)
                elif '40toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.011081055034051624
                    df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi17)
            if 'Summer16' in folder.name:
                if '20to40' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.0087894013882300163
                    df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi16)
                elif '20toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.085869509958513487
                    df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi16)
                elif '40toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.011694009018159155
                    df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi16)
print("Done !")

tlg.cuts(df_gjet)
df_gjet_tagged = tlg.gjet_tagging([df_gjet])
df_gjet_sup = df_gjet_tagged.query('MaxPhotonID > -0.2 and MinPhotonID > -0.2')
df_gjet_inf = df_gjet_tagged.query('MaxPhotonID > -0.2 and MinPhotonID < -0.2')

features = ['MinPhotonID', 'photonPt_fake', 'photonEta_fake', 'n_jets', 'nvtx', 'diphoPt:mass']

weight_sup = df_gjet_sup['weight'].to_numpy()
weight_sup_norm = weight_sup / weight_sup.sum()
weight_inf = df_gjet_inf['weight']
weight_inf_norm = weight_inf / weight_inf.sum()

variables_sup = df_gjet_sup[features].T.to_numpy()
variables_inf = df_gjet_inf[features].T.to_numpy()
variables_sup[1] = np.clip(variables_sup[1], None, 200)
variables_inf[1] = np.clip(variables_inf[1], None, 200)

savepath = 'plots/GJets_pythia_1617/inputs_comparison/'
bins = [20, 20, 20, 6, 20, 20]
ranges = [None, None, None, [1.5, 7.5], [0, 80], [0, 3]]
labels = ['MinPhotonID', 'MinPhoton pT[GeV]', 'MinPhoton eta', 'MinPhoton Nb jets', 'MinPhoton Nb Vtx',
          'Dipho pT / mass']

for i in range(1, len(variables_sup)):
    plt.figure()
    plt.hist(variables_sup[i], bins=bins[i], range=ranges[i], weights=weight_sup_norm,
             histtype='step', label='MinPhotonID > -0.2')
    plt.hist(variables_inf[i], bins=bins[i], range=ranges[i], weights=weight_inf_norm,
             histtype='step', label='MinPhotonID < -0.2')
    plt.xlabel(labels[i])
    plt.ylabel('Normalized entries')
    if features[i] == 'photonPt_fake':
        plt.yscale('log')
    plt.legend()
    plt.savefig('{:s}inputsComparison_{:s}.png'.format(savepath, features[i]))
    plt.close()
