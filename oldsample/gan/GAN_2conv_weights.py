from __future__ import print_function, division
import os
from time import time
import numpy as np
import matplotlib.pyplot as plt
import pandas
from keras.models import Sequential, Model, load_model
from keras.layers import Dense, Input, concatenate, Conv2D, Conv2DTranspose, Reshape, Flatten
from keras.layers.advanced_activations import LeakyReLU
from tensorflow.keras.optimizers import Adam
import toolsGAN as tlg
import ecalic.cmsStyle


class GAN:
    def __init__(self):
        np.random.seed(int(time()))

        # self.features = ['MinPhotonID', 'photonPt_fake', 'photonEta_fake', 'n_jets', 'nvtx', 'diphoPt:mass', 'weight']
        self.features = ['MinPhotonID', 'photonPt_fake', 'photonEta_fake', 'n_jets', 'nvtx', 'weight']
        self.feat_dim = len(self.features)
        self.latent_dim = self.feat_dim - 1

        self.genModelName = "genModel.h5"
        self.discModelName = "discModel.h5"
        self.ganModelName = "ganModel.h5"
        self.model_savepath = 'models/GJets_pythia_161718/Model_withoutDiphoPt/'
        self.plot_savepath = 'plots/GJets_pythia_161718/Model_withoutDiphoPt/'

        self.optimizer = Adam(epsilon=0.1)

        # here is a nice place define combined model
        self.generator = self.generatorbuild()
        self.discriminator = self.discriminatorbuild()
        self.discriminator.trainable = False

        noise = Input(shape=(self.latent_dim,))
        photon_variables = Input(shape=(self.feat_dim - 1,))
        gen_event = self.generator([noise, photon_variables])
        gan_output = self.discriminator(gen_event)

        self.gan = Model(inputs=[noise, photon_variables], outputs=gan_output)
        self.gan.compile(loss='binary_crossentropy', optimizer=self.optimizer)

    def generatorbuild(self):
        generator = Sequential()

        generator.add(Dense(256, input_dim=2 * self.latent_dim))
        generator.add(LeakyReLU(0.2))

        generator.add(Reshape((8, 8, 4)))

        generator.add(Conv2D(64, 2, padding='same'))
        generator.add(LeakyReLU(0.2))

        generator.add(Conv2DTranspose(32, 2, padding='same'))
        generator.add(LeakyReLU(0.2))

        generator.add(Conv2DTranspose(16, 3, padding='same'))
        generator.add(LeakyReLU(0.2))

        generator.add(Flatten())
        generator.add(Dense(1, activation='tanh'))
        generator.summary()

        photon_variables = Input(shape=(self.feat_dim - 1,))
        noise = Input(shape=(self.latent_dim,))
        gen_input = concatenate([noise, photon_variables], axis=1)
        gen_photonid = generator(gen_input)
        gen_event = concatenate([gen_photonid, photon_variables], axis=1)

        model = Model(inputs=[noise, photon_variables], outputs=gen_event)
        return model

    def discriminatorbuild(self):
        discriminator = Sequential()

        discriminator.add(Dense(256, input_dim=self.feat_dim))
        discriminator.add(LeakyReLU(0.2))

        discriminator.add(Reshape((8, 8, 4)))

        discriminator.add(Conv2D(64, 2, padding='same'))
        discriminator.add(LeakyReLU(0.2))

        discriminator.add(Conv2DTranspose(32, 2, padding='same'))
        discriminator.add(LeakyReLU(0.2))

        discriminator.add(Conv2DTranspose(16, 3, padding='same'))
        discriminator.add(LeakyReLU(0.2))

        discriminator.add(Flatten())
        discriminator.add(Dense(1, activation='sigmoid'))

        discriminator.compile(loss='binary_crossentropy', optimizer=self.optimizer)

        discriminator.summary()
        return discriminator

    def train(self, training_df, epochs, batch_size=256, steps=100, saveloss=True):
        # Generate samples
        df = training_df[self.features]
        df_weight = df['weight']
        df_minmax = self.get_minmax(df)
        df_norm = (df - (df.max() + df.min()) / 2) * 2 / (df.max() - df.min())

        # training sample
        df_train = df_norm.sample(frac=0.75, random_state=0)
        x_train = df_train.to_numpy()

        # test sample
        df_test_norm = df_norm.drop(df_train.index)
        df_weight_test = df_weight.drop(df_train.index)
        df_test = df_test_norm * ((df_minmax.loc['max'] - df_minmax.loc['min']) / 2) + (df_minmax.loc['max'] +
                                                                                        df_minmax.loc['min']) / 2
        x_test = df_test.to_numpy()
        x_weight = df_weight_test.to_numpy()
        # x_test = df.to_numpy()
        # x_weight = df_weight.to_numpy()

        # store values for a plot of the loss functions
        epoch_values = []
        dloss_values = []
        gloss_values = []

        # training
        for epoch in range(epochs):
            plot_dloss = 0
            plot_gloss = 0
            for batch in range(steps):
                # Train the discriminator
                half_batch = int(batch_size / 2)
                _, photon_variables, noise = self.create_inputs(x_train, half_batch)
                fake_events = self.generator.predict([noise, photon_variables])

                real_events = x_train[np.random.randint(0, x_train.shape[0], size=half_batch)]

                events = np.concatenate((real_events, fake_events))

                disc_y = np.concatenate((np.ones(half_batch), np.zeros(half_batch)))
                disc_y[half_batch:] -= 0.05 * np.random.randint(0, 2, half_batch)

                d_loss = self.discriminator.train_on_batch(events, disc_y)

                # Train the generator
                _, photon_variables2, noise2 = self.create_inputs(x_train, batch_size)
                y_gen = np.ones(batch_size)
                g_loss = self.gan.train_on_batch([noise2, photon_variables2], y_gen)

                if batch == steps-1:
                    print(f'Epoch: {epoch} \t Discriminator Loss: {d_loss} \t\t Generator Loss: {g_loss}')
                    plot_dloss = d_loss
                    plot_gloss = g_loss

            epoch_values.append(epoch)
            dloss_values.append(plot_dloss)
            gloss_values.append(plot_gloss)

            if (epoch + 1) % 2000 == 0:
                df_photonid_norm = self.create_gen_sample(self.generator, df_test_norm)
                df_photonid = df_photonid_norm * ((df_minmax.loc['max'] - df_minmax.loc['min']) / 2) + (
                        df_minmax.loc['max'] + df_minmax.loc['min']) / 2
                plot_photonid = df_photonid['MinPhotonID'].T.to_numpy()

                plt.figure()
                plt.hist(x_test[:, 0], 20, weights=x_weight, histtype='step', label='Real')
                plt.hist(plot_photonid, 20, weights=x_weight, histtype='step', label='Generated')
                plt.xlabel('PhotonID')
                plt.ylabel('Entries')
                plt.legend(loc='upper right')
                plt.savefig('{:s}1D_photonID_{:d}.png'.format(self.plot_savepath, epoch + 1))
                plt.close()

        self.savemodels()

        # create generated sample
        df_gen_norm = self.create_gen_sample(self.generator, df_test_norm)
        # df_gen_norm = self.create_gen_sample(self.generator, df_norm)
        df_gen = df_gen_norm * ((df_minmax.loc['max'] - df_minmax.loc['min']) / 2) + (df_minmax.loc['max'] +
                                                                                      df_minmax.loc['min']) / 2
        x_gen = df_gen.to_numpy()

        # plot correlation matrix

        self.plotcorr(df_norm, 'Real', 'pearson')
        self.plotcorr(df_gen_norm, 'Gen', 'pearson')

        self.plotfeatures2d(x_test, 'real', x_weight)
        self.plotfeatures2d(x_gen, 'gen', x_weight)

        if saveloss:
            plt.figure()
            plt.plot(epoch_values, dloss_values, label='Discriminator')
            plt.plot(epoch_values, gloss_values, label='Generator')
            plt.title('Loss functions evolution')
            plt.xlabel('Epoch')
            plt.ylabel('Loss')
            plt.legend(loc='upper right')
            plt.savefig(self.plot_savepath + "loss_evolution.png")
            plt.close()

    def get_minmax(self, df):
        df_minmax = pandas.DataFrame(index=['min', 'max'], columns=self.features)
        df_minmax.loc['min'] = df.min()
        df_minmax.loc['max'] = df.max()
        return df_minmax

    def create_inputs(self, sample, size):
        fake_x = sample[np.random.randint(0, sample.shape[0], size=size)]
        photon_variables = fake_x[:, 1:]
        noise = np.random.normal(0, 1, size=(size, self.latent_dim))
        return fake_x, photon_variables, noise

    def create_gen_sample(self, generator, df):
        df_gen_norm = df.drop(columns=['MinPhotonID'])
        noise_test = np.random.normal(0, 1, size=(len(df_gen_norm.index), self.latent_dim))
        gen_events = generator.predict([noise_test, df_gen_norm.to_numpy()])
        df_gen_norm['MinPhotonID'] = gen_events[:, 0]
        df_gen_norm = df_gen_norm[self.features]
        return df_gen_norm

    def plotcorr(self, df, title, corr_type='kendall', savepath=None):
        if savepath is None:
            savepath = self.plot_savepath

        correlations = df.corr(corr_type).abs()
        plt.figure(figsize=(1200 / 96, 720 / 96), dpi=96)
        im, _ = tlg.heatmap(correlations, self.features, self.features, origin='lower', cmap='YlOrRd')
        tlg.annotate_heatmap(im)
        plt.savefig("{:s}corr{:s}.png".format(savepath, title))
        plt.close()

    def plotfeatures2d(self, arrsamp, title, df_weight, bins=None, ranges=None, savepath=None):
        if savepath is None:
            savepath = self.plot_savepath
        if bins is None:
            bins = [10, 10, 10, 6, 10, 10, 10]
        if ranges is None:
            ranges = [[-0.2, 1], [25, 125], None, [1.5, 7.5], [1, 80], [0, 2], [0, 2]]

        for i in range(1, len(self.features)):
            tlg.plot2d_meanproj(arrsamp[:, 0], arrsamp[:, i], self.features[i], df_weight,
                                bins=(bins[0], bins[i]), range=[ranges[0], ranges[i]])
            plt.savefig("{:s}2D{:s}_{:d}.png".format(savepath, title, i))
            plt.close()

    def plotmeanoverlay(self, x_real, ganed_id, df_weight, bins=None, ranges=None, savepath=None):
        if savepath is None:
            savepath = self.plot_savepath
        if bins is None:
            bins = [10, 10, 10, 6, 10, 10, 10]
        if ranges is None:
            ranges = [[-0.2, 1], [25, 125], None, [1.5, 7.5], [1, 80], [0, 2], [0, 2]]

        for i in range(1, len(self.features)):
            tlg.mean_overlay(x_real[:, 0], ganed_id, x_real[:, i], df_weight, self.features[i],
                             bins=(bins[0], bins[i]), range=[ranges[0], ranges[i]])
            plt.savefig("{:s}meanoverlay_{:d}.png".format(savepath, i))
            plt.close()

    def savemodels(self):
        self.generator.save(self.model_savepath + self.genModelName)
        self.discriminator.save(self.model_savepath + self.discModelName)
        self.gan.save(self.model_savepath + self.ganModelName)

    def evaluate(self, sample, training):
        # Load the chosen generator model
        generator = load_model(self.model_savepath + self.genModelName)

        # Create samples
        df = sample[self.features]
        x_weight = df['weight'].to_numpy()
        df_minmax = self.get_minmax(df)
        df_norm = (df - (df.max() + df.min()) / 2) * 2 / (df.max() - df.min())

        # Evaluation sample
        # x_eval = df.to_numpy()
        x_eval = df_norm.to_numpy()

        # Generated sample
        df_gen_norm = self.create_gen_sample(generator, df_norm)
        df_gen = df_gen_norm * ((df_minmax.loc['max'] - df_minmax.loc['min']) / 2) + (df_minmax.loc['max'] +
                                                                                      df_minmax.loc['min']) / 2
        # x_gen = df_gen.to_numpy()
        x_gen = df_gen_norm.to_numpy()

        # Comparison with real distribution
        real_photonid = training['MinPhotonID']
        real_weight_norm = training['weight'] / training['weight'].sum()

        gen_photonid = df_gen_norm['MinPhotonID'] * 0.6 + 0.4
        gen_weight_norm = x_weight / x_weight.sum()

        # Plots
        save = 'plots/GJets_pythia_161718/Model_withoutDiphoPt/eval_1617/'

        plt.figure()
        plt.hist(real_photonid.to_numpy(), 20, range=[-0.2, 1], weights=real_weight_norm.to_numpy(),
                 histtype='step', label='True MinPhotonID < -0.2')
        plt.hist(gen_photonid.to_numpy(), 20, range=[-0.2, 1], weights=gen_weight_norm,
                 histtype='step', label='GANed from MinPhotonID > -0.2')
        plt.xlabel('PhotonID')
        plt.ylabel('Normalized entries')
        plt.legend(loc='upper right')
        plt.savefig(save + '1D_photonID_comparison.png')
        plt.close()

        """
        bins = [10, 10, 10, 6, 10, 10, 10]
        ranges = [None, [25, 125], None, [1.5, 7.5], [0.5, 80], [0, 2], [0, 1]]

        plt.figure()
        plt.hist(x_eval[:, 0], 20, weights=x_weight, histtype='step', label='True')
        plt.hist(x_gen[:, 0], 20, weights=x_weight, histtype='step', label='GANed')
        plt.xlabel('PhotonID')
        plt.ylabel('Entries')
        plt.legend(loc='upper right')
        plt.savefig(save + '1D_photonID_eval.png')
        plt.close()

        self.plotcorr(df_norm, 'True', savepath=save)
        self.plotcorr(df_gen_norm, 'GANed', savepath=save)

        self.plotfeatures2d(x_eval, 'True', x_weight, bins=bins, ranges=ranges, savepath=save)
        self.plotfeatures2d(x_gen, 'GANed', x_weight, bins=bins, ranges=ranges, savepath=save)

        self.plotmeanoverlay(x_eval, x_gen[:, 0], x_weight, bins=bins, ranges=ranges, savepath=save)
        """


if __name__ == '__main__':
    lumi16 = 35.9
    lumi17 = 41.5
    lumi18 = 59.8

    df_gjet = pandas.DataFrame()
    completion = 0
    print("Extracting data :")
    for folder in os.scandir("../ntuples"):
        for filename in os.scandir(folder):
            completion += 1
            print(int((completion / 69) * 100), "%")
            if 'GJet_' in folder.name:
                if 'Summer16' in folder.name:
                    if '20to40' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.0087894013882300163
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi16)
                    elif '20toInf' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.085869509958513487
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi16)
                    elif '40toInf' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.011694009018159155
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi16)
                elif 'Fall17' in folder.name:
                    if '20to40' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.013174044112514596
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi17)
                    elif '20toInf' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.075237801398417098
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi17)
                    elif '40toInf' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.011081055034051624
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi17)
                """
                elif 'Autumn18' in folder.name:
                    if '20to40' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.0166472687
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi18)
                    elif '20toInf' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.5247033845
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi18)
                    elif '40toInf' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.0881669793
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi18)
                """
    print("Done !")

    tlg.cuts(df_gjet)
    df_gjet_tagged = tlg.gjet_tagging([df_gjet])
    df_gjet_training = df_gjet_tagged.query('MaxPhotonID > -0.2 and MinPhotonID > -0.2')
    df_gjet_evaluate = df_gjet_tagged.query('MaxPhotonID > -0.2 and MinPhotonID < -0.2')

    gan = GAN()
    # gan.train(df_gjet_training, epochs=2000, batch_size=50, steps=565)
    gan.evaluate(df_gjet_evaluate, df_gjet_training)
