from __future__ import print_function, division
import os
from time import time
import numpy as np
import matplotlib.pyplot as plt
import pandas
from keras.models import Sequential, Model
from keras.layers import Dense, Input, concatenate
from keras.layers.advanced_activations import LeakyReLU
from tensorflow.keras.optimizers import Adam
import toolsGAN as tlg


class GAN:
    def __init__(self):
        np.random.seed(int(time()))

        self.features = ['MinPhotonID', 'photonPt_fake', 'photonEta_fake', 'n_jets', 'nvtx', 'diphoPt:mass']
        self.feat_dim = len(self.features)
        self.latent_dim = self.feat_dim - 1

        self.genModelName = "genModel.h5"
        self.discModelName = "discModel.h5"
        self.ganModelName = "ganModel.h5"

        self.optimizer = Adam(epsilon=0.1)

        # here is a nice place define combined model
        self.generator = self.generatorbuild()
        self.discriminator = self.discriminatorbuild()

        self.discriminator.trainable = False
        gan_input = Input(shape=(2 * self.latent_dim,))
        gen_photonid = self.generator(gan_input)
        photon_variables = Input(shape=(self.feat_dim - 1, ))
        disc_input = concatenate([gen_photonid, photon_variables])
        gan_output = self.discriminator(disc_input)

        self.gan = Model(inputs=[gan_input, photon_variables], outputs=gan_output)
        self.gan.compile(loss='binary_crossentropy', optimizer=self.optimizer)

    def generatorbuild(self):
        generator = Sequential()

        generator.add(Dense(256, input_dim=2 * self.latent_dim))
        generator.add(LeakyReLU(0.2))

        generator.add(Dense(128))
        generator.add(LeakyReLU(0.2))

        generator.add(Dense(64))
        generator.add(LeakyReLU(0.2))

        generator.add(Dense(1, activation='tanh'))

        generator.compile(loss='mean_squared_error', optimizer=self.optimizer)
        generator.summary()
        return generator

    def discriminatorbuild(self):
        discriminator = Sequential()

        discriminator.add(Dense(256, input_dim=self.feat_dim))
        discriminator.add(LeakyReLU(0.2))

        discriminator.add(Dense(128))
        discriminator.add(LeakyReLU(0.2))

        discriminator.add(Dense(64))
        discriminator.add(LeakyReLU(0.2))

        discriminator.add(Dense(1, activation='sigmoid'))

        discriminator.compile(loss='binary_crossentropy', optimizer=self.optimizer)
        discriminator.summary()
        return discriminator

    def train(self, training_df, epochs, batch_size=256, steps=100, show=False):
        # Generate samples
        df = training_df[self.features + ['weight']]
        minphotonid = df['MinPhotonID']
        df_weight = df['weight']
        df = df.drop('weight', axis=1)
        df_norm = (df - df.mean()) * 2 / (df.max() - df.min())
        df_norm['MinPhotonID'] = minphotonid

        # training sample
        df_train = df_norm.sample(frac=0.75, random_state=0)
        x_train = df_train.to_numpy()

        # test sample
        df_test = df_norm.drop(df_train.index)
        df_weight_test = df_weight.drop(df_train.index)
        x_test = df_test.to_numpy()
        x_weight = df_weight_test.to_numpy()

        # store values for a plot of the loss functions
        epoch_values = []
        dloss_values = []
        gloss_values = []

        # training
        for epoch in range(epochs):
            plot_dloss = 0
            plot_gloss = 0
            for batch in range(steps):

                fake_x = x_train[np.random.randint(0, x_train.shape[0], size=batch_size)]
                photon_variables = fake_x[:, 1:]
                noise = np.random.normal(0, 1, size=(batch_size, self.latent_dim))
                gen_input = np.concatenate((noise, photon_variables), axis=1)
                fake_photonid = self.generator.predict(gen_input)
                fake_x[:, 0] = fake_photonid.T
                real_x = x_train[np.random.randint(0, x_train.shape[0], size=batch_size)]

                x = np.concatenate((real_x, fake_x))

                disc_y = np.concatenate((np.ones(batch_size), np.zeros(batch_size)))
                disc_y[batch_size:] -= 0.05 * np.random.randint(0, 2, batch_size)

                # Train the discriminator
                d_loss = self.discriminator.train_on_batch(x, disc_y)

                # Train the generator
                fake_x2 = x_train[np.random.randint(0, x_train.shape[0], size=batch_size)]
                photon_variables2 = fake_x2[:, 1:]
                noise2 = np.random.normal(0, 1, size=(batch_size, self.latent_dim))
                gen_input2 = np.concatenate((noise2, photon_variables2), axis=1)
                y_gen = np.ones(batch_size)
                g_loss = self.gan.train_on_batch([gen_input2, photon_variables], y_gen)

                if batch == 0:
                    print(f'Epoch: {epoch} \t Discriminator Loss: {d_loss} \t\t Generator Loss: {g_loss}')
                    plot_dloss = d_loss
                    plot_gloss = g_loss

            epoch_values.append(epoch)
            dloss_values.append(plot_dloss)
            gloss_values.append(plot_gloss)

        # create generated sample
        df_gen = df_test.drop(columns=['MinPhotonID'])
        rand = np.random.normal(0, 1, size=(len(df_gen.index), self.latent_dim))
        inputs = np.concatenate((rand, df_gen.to_numpy()), axis=1)
        photonid_gen = self.generator.predict(inputs)
        df_gen['MinPhotonID'] = photonid_gen
        df_gen = df_gen[self.features]
        x_gen = df_gen.to_numpy()

        # plot correlation matrix
        real_correlations = df_test.corr('kendall')
        gen_correlations = df_gen.corr('kendall')

        savepath = 'plots/GJets_161718/Model_2/'

        plt.figure(figsize=(1200 / 96, 720 / 96), dpi=96)
        plt.hist(df_test['MinPhotonID'].T.to_numpy(), 20, weights=df_weight_test.T.to_numpy(),
                 histtype='step', label='Real')
        plt.hist(df_gen['MinPhotonID'].T.to_numpy(), 20, weights=df_weight_test.T.to_numpy(),
                 histtype='step', label='Generated')
        plt.xlabel('PhotonID')
        plt.ylabel('Entries')
        plt.legend(loc='upper left')
        plt.savefig(savepath + '1D_photonID.png')
        plt.close()

        plt.figure(figsize=(1200 / 96, 720 / 96), dpi=96)
        im, _ = tlg.heatmap(real_correlations, self.features, self.features, origin='lower')
        tlg.annotate_heatmap(im)
        plt.savefig(savepath + "corrReal.png")
        plt.close()

        self.plotfeatures2d(x_test, 'real', x_weight)

        plt.figure(figsize=(1200 / 96, 720 / 96), dpi=96)
        im, _ = tlg.heatmap(gen_correlations, self.features, self.features, origin='lower')
        tlg.annotate_heatmap(im)
        plt.savefig(savepath + "corrGen.png")
        plt.close()

        self.plotfeatures2d(x_gen, 'fake', x_weight)

        if show:
            plt.figure(figsize=(1200 / 96, 720 / 96), dpi=96)
            plt.plot(epoch_values, dloss_values, label='Discriminator')
            plt.plot(epoch_values, gloss_values, label='Generator')
            plt.title('Loss functions evolution')
            plt.xlabel('Epoch')
            plt.ylabel('Loss')
            plt.legend(loc='upper right')
            plt.savefig(savepath + "loss_evolution.png")
            plt.close()

    # def evaluate(self):

    def plotfeatures2d(self, arrsamp, title, df_weight):

        for i in range(1, len(self.features)):
            tlg.plot2d_meanproj(arrsamp[:, 0], arrsamp[:, i], self.features[i], df_weight)
            plt.savefig(r"plots/GJets_161718/Model_2/2D%s_%d.png" % (title, i))
            plt.close()

    def savemodels(self):
        savepath = 'models/GJets_161718/Model_2/'
        self.generator.save(savepath + self.genModelName)
        self.discriminator.save(savepath + self.discModelName)
        self.gan.save(savepath + self.ganModelName)


if __name__ == '__main__':
    luminosity = 41.5

    df_gjet = pandas.DataFrame()
    completion = 0
    print("Extracting data :")
    for folder in os.scandir("../ntuples"):
        for filename in os.scandir(folder):
            completion += 1
            print(int((completion / 69) * 100), "%")
            if 'GJets_' in folder.name:
                if 'Fall17' in folder.name:
                    if '40To100' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 3.346080992393051
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * luminosity)
                    elif '100To200' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.86681778146470212
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * luminosity)
                    elif '200To400' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.11795314318135013
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * luminosity)
                    elif '400To600' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.055537261041074731
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * luminosity)
                    elif '600ToInf' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.022394866068735982
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * luminosity)
                elif 'Autumn18' in folder.name:
                    if '40To100' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 1.9893071004836207
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * luminosity)
                    elif '100To200' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.88113062363417383
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * luminosity)
                    elif '200To400' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.11468868362468057
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * luminosity)
                    elif '400To600' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.055407883724490792
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * luminosity)
                    elif '600ToInf' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.017182858089356656
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * luminosity)
                elif 'Summer16' in folder.name:
                    if '40To100' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 2.476909254730173
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * luminosity)
                    elif '100To200' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.90160928845608568
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * luminosity)
                    elif '200To400' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.11107048269770327
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * luminosity)
                    elif '400To600' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.053951822800870346
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * luminosity)
                    elif '600ToInf' in folder.name:
                        print("Extracting from " + folder.name)
                        scale1fb = 0.018599224500906047
                        df_gjet = tlg.extract(df_gjet, filename, scale1fb * luminosity)
    print("Done !")

    tlg.cuts(df_gjet)
    df_gjet.query('(MaxPhotonID > -0.2) and (MinPhotonID > -0.2)', inplace=True)
    gan = GAN()
    gan.train(df_gjet, epochs=25, batch_size=50, steps=517, show=True)
    gan.savemodels()
