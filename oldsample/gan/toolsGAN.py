import numpy as np
from matplotlib import ticker
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import uproot as up
import pandas
from keras.losses import BinaryCrossentropy


def extract(dataframe, file, scalefactor=1.0, branch="tthHadronicTagDumper/trees/tth_13TeV_all", features=None):

    if features is None:
        features = ['leadIDMVA', 'subleadIDMVA', 'leadPt', 'subleadPt', 'leadEta', 'subleadEta',
                    'leadGenMatch', 'subleadGenMatch', 'n_jets', 'nb_loose', 'nvtx', 'dipho_sumpt', 'mass', 'weight']

    events = up.open(file)[branch]
    tmp_df = events.pandas.df(features)
    tmp_df['weight'] = tmp_df['weight'] * scalefactor
    tmp_df['diphoPt:mass'] = tmp_df['dipho_sumpt'] / tmp_df['mass']
    tmp_df['MaxPhotonID'] = tmp_df['leadIDMVA'].where(tmp_df['leadIDMVA'] >= tmp_df['subleadIDMVA'],
                                                      tmp_df['subleadIDMVA'])
    tmp_df['MinPhotonID'] = tmp_df['subleadIDMVA'].where(tmp_df['leadIDMVA'] >= tmp_df['subleadIDMVA'],
                                                         tmp_df['leadIDMVA'])
    tmp_df['photonPt'] = tmp_df['leadPt'].where(tmp_df['leadIDMVA'] == tmp_df['MaxPhotonID'], tmp_df['subleadPt'])
    tmp_df['photonEta'] = tmp_df['leadEta'].where(tmp_df['leadIDMVA'] == tmp_df['MaxPhotonID'], tmp_df['subleadEta'])
    tmp_df['photonPt_fake'] = tmp_df['subleadPt'].where(tmp_df['leadIDMVA'] == tmp_df['MaxPhotonID'], tmp_df['leadPt'])
    tmp_df['photonEta_fake'] = tmp_df['subleadEta'].where(tmp_df['leadIDMVA'] == tmp_df['MaxPhotonID'],
                                                          tmp_df['leadEta'])

    return dataframe.append(tmp_df, ignore_index=True)


def cuts(df):
    cutmass = '(mass > 100 and mass < 115) or (mass > 135 and mass < 180)'
    cutpt = 'leadPt > 35 and subleadPt > 25'
    cuteta = 'leadEta > -2.5 and leadEta < 2.5 and subleadEta > -2.5 and subleadEta < 2.5'
    df.query(cutmass, inplace=True)
    df.query(cutpt, inplace=True)
    df.query(cuteta, inplace=True)
    return df


def gjet_tagging(dataframes):
    df = pandas.DataFrame()
    for i in range(len(dataframes)):
        tmp_df = dataframes[i].query('(leadGenMatch != 1 and subleadGenMatch == 1) or '
                                     '(leadGenMatch == 1 and subleadGenMatch != 1)')
        df = df.append(tmp_df, ignore_index=True)
    return df


def heatmap(data, row_labels, col_labels, ax=None, cbar_kw=None, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (N, M).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if cbar_kw is None:
        cbar_kw = {}

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, norm=colors.LogNorm(vmin=data.min().min(), vmax=data.max().max()), **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=25, ha="right",
             rotation_mode="anchor")

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar


def annotate_heatmap(im, data=None, valfmt="{x:.2f}", textcolors=None, threshold=None, **textkw):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A list or array of two color specifications.  The first is used for
        values below a threshold, the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **textkw
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if textcolors is None:
        textcolors = ["black", "white"]

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[im.norm(data[i, j]) > threshold])
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts


def plot2d_meanproj(x, y, xlabel, df_weight, **kwargs):

    nullfmt = ticker.NullFormatter()

    # definitions for the axes
    left, width = 0.1, 0.65
    bottom, height = 0.1, 0.65
    bottom_h = left_h = left + width + 0.02

    rect_2dhisto = [left, bottom, width, height]
    rect_scatx = [left, bottom_h, width, 0.2]
    rect_scaty = [left_h, bottom, 0.1, height]

    # start with a rectangular Figure
    fig = plt.figure(1, figsize=(8, 8))

    ax2dhisto = plt.axes(rect_2dhisto)
    plt.xlabel(xlabel)
    plt.ylabel('MinPhotonID')
    axscatx = plt.axes(rect_scatx)
    plt.ylabel('Mean')
    axcolorbar = plt.axes(rect_scaty)

    # no labels
    axscatx.xaxis.set_major_formatter(nullfmt)

    # the 2dhoist plot:
    hist, edgesx, edgesy = np.histogram2d(x, y, weights=df_weight, **kwargs)
    cm = ax2dhisto.pcolormesh(edgesy, edgesx, hist, cmap='YlOrRd')
    fig.colorbar(cm, cax=axcolorbar)

    # the scatter plots:
    xscatter = (edgesy[:-1]+edgesy[1:])/2.
    y = (edgesx[:-1]+edgesx[1:])/2.

    yscatter = []
    for column in range(np.shape(hist)[1]):
        s = 0
        for row in range(np.shape(hist)[0]):
            s += hist[row][column] * y[row]
        yscatter.append(s / hist[:, column].sum())

    axscatx.scatter(xscatter, yscatter)

    axscatx.set_xlim(ax2dhisto.get_xlim())

    return fig


def mean_overlay(real, gen, variable, weight, xlabel, **kwargs):
    hist1, edgesx1, edgesy1 = np.histogram2d(real, variable, weights=weight, **kwargs)
    hist2, edgesx2, edgesy2 = np.histogram2d(gen, variable, weights=weight, **kwargs)

    xscatter1 = (edgesy1[:-1] + edgesy1[1:]) / 2.
    y1 = (edgesx1[:-1] + edgesx1[1:]) / 2.

    xscatter2 = (edgesy2[:-1] + edgesy2[1:]) / 2.
    y2 = (edgesx2[:-1] + edgesx2[1:]) / 2.

    yscatter1 = []
    for column in range(np.shape(hist1)[1]):
        s = 0
        for row in range(np.shape(hist1)[0]):
            s += hist1[row][column] * y1[row]
        yscatter1.append(s / hist1[:, column].sum())

    yscatter2 = []
    for column in range(np.shape(hist2)[1]):
        s = 0
        for row in range(np.shape(hist2)[0]):
            s += hist2[row][column] * y2[row]
        yscatter2.append(s / hist2[:, column].sum())

    fig = plt.figure()
    plt.scatter(xscatter1, yscatter1, label='True')
    plt.scatter(xscatter2, yscatter2, label='GANed')
    plt.ylim(0, 0.6)
    plt.xlim(edgesy1[0], edgesy1[-1])
    plt.xlabel(xlabel)
    plt.ylabel('Mean of PhotonID')
    plt.legend()

    return fig


def loss_func(idreal, idgen, pt):
    alpha = 1
    mse = abs((pt - idreal) ** 2 - (pt - idgen) ** 2)

    def loss(y_true, y_pred):
        return BinaryCrossentropy()(y_true, y_pred) + alpha * mse

    return loss


if __name__ == '__main__':
    print('Library of useful functions !')
