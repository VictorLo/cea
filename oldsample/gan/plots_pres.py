import ecalic.cmsStyle
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas
from keras.models import load_model
import toolsGAN as tlg


def get_minmax(dataframe, feat):
    df_minmax = pandas.DataFrame(index=['min', 'max'], columns=feat)
    df_minmax.loc['min'] = dataframe.min()
    df_minmax.loc['max'] = dataframe.max()
    return df_minmax


def create_gen_sample(gen, df_training):
    all_feat = ['MinPhotonID', 'photonPt_fake', 'photonEta_fake', 'n_jets', 'nvtx', 'diphoPt:mass', 'weight']
    feat = ['photonPt_fake', 'photonEta_fake', 'n_jets', 'nvtx', 'diphoPt:mass', 'weight']
    latent_dim = len(feat)

    dataframe = df_training[feat]
    df_minmax = get_minmax(dataframe, feat)
    df_gen_norm = (dataframe - (dataframe.max() + dataframe.min()) / 2) * 2 / (dataframe.max() - dataframe.min())
    noise_test = np.random.normal(0, 1, size=(len(df_gen_norm.index), latent_dim))
    gen_events = gen.predict([noise_test, df_gen_norm.to_numpy()])
    df_gen = df_gen_norm * ((df_minmax.loc['max'] - df_minmax.loc['min']) / 2) + (df_minmax.loc['max'] +
                                                                                  df_minmax.loc['min']) / 2
    df_gen['MinPhotonID'] = gen_events[:, 0] * 0.6 + 0.4
    df_gen = df_gen[all_feat]
    return df_gen


lumi16 = 35.9
lumi17 = 41.5

df_gjet = pandas.DataFrame()
completion = 0
print("Extracting data :")
for folder in os.scandir("../ntuples"):
    for filename in os.scandir(folder):
        completion += 1
        print(int((completion / 69) * 100), "%")
        if 'GJet_' in folder.name:
            if 'Fall17' in folder.name:
                if '20to40' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.013174044112514596
                    df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi17)
                elif '20toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.075237801398417098
                    df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi17)
                elif '40toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.011081055034051624
                    df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi17)
            if 'Summer16' in folder.name:
                if '20to40' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.0087894013882300163
                    df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi16)
                elif '20toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.085869509958513487
                    df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi16)
                elif '40toInf' in folder.name:
                    print("Extracting from " + folder.name)
                    scale1fb = 0.011694009018159155
                    df_gjet = tlg.extract(df_gjet, filename, scale1fb * lumi16)
print("Done !")

# Global variables
save = 'plot_pres/'
features = ['MinPhotonID', 'photonPt_fake', 'photonEta_fake', 'n_jets', 'nvtx']

# Cuts and photonID selections
tlg.cuts(df_gjet)
df_gjet_tagged = tlg.gjet_tagging([df_gjet])
df_gjet_training = df_gjet_tagged.query('MaxPhotonID > -0.2 and MinPhotonID > -0.2')
df_gjet_evaluate = df_gjet_tagged.query('MaxPhotonID > -0.2 and MinPhotonID < -0.2')
print(len(df_gjet_training.index))
print(len(df_gjet_evaluate.index))

df = df_gjet_training[features]
nb_jets = df_gjet_training['nb_loose'].to_numpy()
weight = df_gjet_training['weight'].to_numpy()

# Load generator model
generator = load_model('models/GJets_pythia_1617/Model_2conv_weights/genModel.h5')

# Plot MinPhotonID
df_minphotonid = df_gjet_training['MinPhotonID'].append(df_gjet_evaluate['MinPhotonID'])
df_minphotonid_w = df_gjet_training['weight'].append(df_gjet_evaluate['weight'])

plt.figure()
plt.hist(df_minphotonid.to_numpy(), bins=20, weights=df_minphotonid_w.to_numpy())
plt.vlines(-0.2, 0, 7500, colors='r', linestyles='dashed')
plt.xticks([-0.9, -0.5, -0.2, 0, 0.5, 1])
plt.xlabel('MinPhotonID')
plt.ylabel('Events')
plt.savefig(save + 'MinPhotonID_distrib.png')
plt.close()

# Plot NJets
njets_1 = df_gjet_training['n_jets'].to_numpy()
nbjets_1 = df_gjet_training['nb_loose'].to_numpy()
njets_2 = df_gjet_evaluate['n_jets'].to_numpy()
nbjets_2 = df_gjet_evaluate['nb_loose'].to_numpy()
w1 = df_gjet_training['weight'].to_numpy()
w2 = df_gjet_evaluate['weight'].to_numpy()
scale = w1.sum() / w2.sum()

plt.figure()
plt.hist(njets_1, bins=7, weights=w1, range=[0.5, 7.5], histtype='step', label='MinPhotonID > -0.2')
plt.hist(njets_2, bins=7, weights=w2 * scale, range=[0.5, 7.5], histtype='step', label='MinPhotonID < -0.2')
plt.xlabel('Number of jets')
plt.ylabel('Events')
plt.legend()
plt.savefig(save + 'njets_regions.png')
plt.close()

plt.figure()
plt.hist(nbjets_1, bins=5, weights=w1, range=[-0.5, 4.5], histtype='step', label='MinPhotonID > -0.2')
plt.hist(nbjets_2, bins=5, weights=w2 * scale, range=[-0.5, 4.5], histtype='step', label='MinPhotonID < -0.2')
plt.xlabel('Number of b jets')
plt.ylabel('Events')
plt.legend()
plt.savefig(save + 'nbjets_regions.png')
plt.close()

# Plot correlation map
correlations = df.corr().abs()
plt.figure(figsize=(1200 / 96, 720 / 96), dpi=96)
im, _ = tlg.heatmap(correlations, features, features, origin='lower', cmap='viridis')
tlg.annotate_heatmap(im, valfmt="{x:.3f}")
plt.savefig(save + 'CorrMap.png')
plt.close()


# Plot 2D correlation
df_ganed = create_gen_sample(generator, df_gjet_training)
ganed_id = df_ganed['MinPhotonID'].to_numpy()

photonid = df['MinPhotonID'].to_numpy()

pt = df['photonPt_fake'].to_numpy()
eta = df['photonEta_fake'].to_numpy()
nJets = df['n_jets'].to_numpy()
nVtx = df['nvtx'].to_numpy()

plt.figure()
hist, edgesx, edgesy = np.histogram2d(photonid, pt, weights=weight, bins=(10, 10), range=[None, [25, 125]])
plt.pcolormesh(edgesy, edgesx, hist, cmap='YlOrRd')
plt.colorbar()
plt.xlabel('Photon pT')
plt.ylabel('MinPhotonID')
plt.savefig(save + '2Dreal_pT.png')
plt.close()

plt.figure()
hist, edgesx, edgesy = np.histogram2d(ganed_id, pt, weights=weight, bins=(10, 10), range=[None, [25, 125]])
plt.pcolormesh(edgesy, edgesx, hist, cmap='YlOrRd')
plt.colorbar()
plt.xlabel('Photon pT')
plt.ylabel('MinPhotonID')
plt.savefig(save + '2Dganed_pT.png')
plt.close()

plt.figure()
hist, edgesx, edgesy = np.histogram2d(photonid, nJets, weights=weight, bins=(10, 6), range=[None, [1.5, 7.5]])
plt.pcolormesh(edgesy, edgesx, hist, cmap='YlOrRd')
plt.colorbar()
plt.xlabel('Number of jets')
plt.ylabel('MinPhotonID')
plt.savefig(save + '2Dreal_nJets.png')
plt.close()

plt.figure()
hist, edgesx, edgesy = np.histogram2d(ganed_id, nJets, weights=weight, bins=(10, 6), range=[None, [1.5, 7.5]])
plt.pcolormesh(edgesy, edgesx, hist, cmap='YlOrRd')
plt.colorbar()
plt.xlabel('Number of jets')
plt.ylabel('MinPhotonID')
plt.savefig(save + '2Dganed_nJets.png')
plt.close()

# Plot photonID distrib
plt.figure()
plt.hist(photonid, bins=20, range=[-0.2, 1], weights=weight, histtype='step', label='True')
plt.hist(ganed_id, bins=20, range=[-0.2, 1], weights=weight, histtype='step', label='GANed')
plt.xlabel('MinPhotonID')
plt.ylabel('Events')
plt.legend()
plt.savefig(save + 'MinPhotonID_ganed.png')
plt.close()

# Plot 2D correlation with mean

tlg.plot2d_meanproj(photonid, pt, 'Photon pT', weight, bins=(10, 10), range=[None, [25, 125]])
plt.savefig(save + '2Dreal_mean_pT.png')
plt.close()

# Plot means overlay

tlg.mean_overlay(photonid, ganed_id, pt, weight, 'Photon pT', bins=(10, 10), range=(None, (25, 125)))
plt.savefig(save + 'mean_pT.png')
plt.close()

tlg.mean_overlay(photonid, ganed_id, eta, weight, 'Photon eta', bins=(10, 10))
plt.savefig(save + 'mean_eta.png')
plt.close()

tlg.mean_overlay(photonid, ganed_id, nJets, weight, 'Number of jets', bins=(10, 6), range=(None, (1.5, 7.5)))
plt.savefig(save + 'mean_nJets.png')
plt.close()

tlg.mean_overlay(photonid, ganed_id, nVtx, weight, 'Number of vertices', bins=(10, 10), range=(None, (0.5, 80)))
plt.savefig(save + 'mean_nVtx.png')
plt.close()

# Plot evaluation of generator
df_ganed_eval = create_gen_sample(generator, df_gjet_evaluate)
ganed_id_eval = df_ganed_eval['MinPhotonID'].to_numpy()
weight_eval = df_gjet_evaluate['weight'].to_numpy()
rescaled = weight_eval * (weight.sum() / weight_eval.sum())

plt.figure()
plt.hist(df_minphotonid.to_numpy(), bins=19, range=[-0.9, 1], weights=df_minphotonid_w.to_numpy(), histtype='step',
         label='True')
plt.hist(ganed_id_eval, bins=12, weights=rescaled, histtype='step', label='GANed')
plt.xticks([-0.9, -0.5, -0.2, 0, 0.5, 1])
plt.xlabel('MinPhotonID')
plt.ylabel('Events')
plt.legend()
plt.savefig(save + 'evaluation.png')
plt.close()
