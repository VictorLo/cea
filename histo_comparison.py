import os
import numpy as np
import matplotlib.pyplot as plt
import uproot as up

my_dpi = 96

lead_id = [[], [], []]
sublead_id = [[], [], []]
lead_pt = [[], [], []]
lead_eta = [[], [], []]
sublead_pt = [[], [], []]
sublead_eta = [[], [], []]
w = [[], [], []]

# data extraction
for dir in os.scandir("ntuples_2"):
    if 'DoubleEMEnriched' in str(dir):
        for filename in os.scandir(dir):
            file = up.open(filename)
            events = file["tagsDumper/trees/_13TeV_TTHHadronicTag"]
            lead_id[0] = np.concatenate((lead_id[0], events.array("leadIDMVA")), axis=None)
            sublead_id[0] = np.concatenate((sublead_id[0], events.array("subleadIDMVA")), axis=None)
            lead_pt[0] = np.concatenate((lead_pt[0], events.array("leadPt")), axis=None)
            sublead_pt[0] = np.concatenate((sublead_pt[0], events.array("subleadPt")), axis=None)
            lead_eta[0] = np.concatenate((lead_eta[0], events.array("leadEta")), axis=None)
            sublead_eta[0] = np.concatenate((sublead_eta[0], events.array("subleadEta")), axis=None)
            w[0] = np.concatenate((w[0], events.array("weight")), axis=None)
    elif 'TuneCUETP8M1' in str(dir):
        for filename in os.scandir(dir):
            file = up.open(filename)
            events = file["tagsDumper/trees/_13TeV_TTHHadronicTag"]
            lead_id[1] = np.concatenate((lead_id[1], events.array("leadIDMVA")), axis=None)
            sublead_id[1] = np.concatenate((sublead_id[1], events.array("subleadIDMVA")), axis=None)
            lead_pt[1] = np.concatenate((lead_pt[1], events.array("leadPt")), axis=None)
            sublead_pt[1] = np.concatenate((sublead_pt[1], events.array("subleadPt")), axis=None)
            lead_eta[1] = np.concatenate((lead_eta[1], events.array("leadEta")), axis=None)
            sublead_eta[1] = np.concatenate((sublead_eta[1], events.array("subleadEta")), axis=None)
            w[1] = np.concatenate((w[1], events.array("weight")), axis=None)
    else:
        for filename in os.scandir(dir):
            file = up.open(filename)
            events = file["tagsDumper/trees/_13TeV_TTHHadronicTag"]
            lead_id[2] = np.concatenate((lead_id[2], events.array("leadIDMVA")), axis=None)
            sublead_id[2] = np.concatenate((sublead_id[2], events.array("subleadIDMVA")), axis=None)
            lead_pt[2] = np.concatenate((lead_pt[2], events.array("leadPt")), axis=None)
            sublead_pt[2] = np.concatenate((sublead_pt[2], events.array("subleadPt")), axis=None)
            lead_eta[2] = np.concatenate((lead_eta[2], events.array("leadEta")), axis=None)
            sublead_eta[2] = np.concatenate((sublead_eta[2], events.array("subleadEta")), axis=None)
            w[2] = np.concatenate((w[2], events.array("weight")), axis=None)

max_id = [[], [], []]
min_id = [[], [], []]
maxid_pt = [[], [], []]
minid_pt = [[], [], []]
maxid_eta = [[], [], []]
minid_eta = [[], [], []]
weight = [[], [], []]
for i in range(len(lead_id)):
    for j in range(len(lead_id[i])):
        if lead_id[i][j] >= sublead_id[i][j] and lead_id[i][j] >= 0.5 and sublead_id[i][j] <= -0.2:
            max_id[i].append(lead_id[i][j])
            min_id[i].append(sublead_id[i][j])
            maxid_pt[i].append(lead_pt[i][j])
            minid_pt[i].append(sublead_pt[i][j])
            maxid_eta[i].append(lead_eta[i][j])
            minid_eta[i].append(sublead_eta[i][j])
            weight[i].append(w[i][j])
        elif lead_id[i][j] < sublead_id[i][j] and sublead_id[i][j] >= 0.5 and lead_id[i][j] <= -0.2:
            max_id[i].append(sublead_id[i][j])
            min_id[i].append(lead_id[i][j])
            maxid_pt[i].append(sublead_pt[i][j])
            minid_pt[i].append(lead_pt[i][j])
            maxid_eta[i].append(sublead_eta[i][j])
            minid_eta[i].append(lead_eta[i][j])
            weight[i].append(w[i][j])

# plot histograms
plt.figure(1, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.subplot(121)
plt.hist(max_id[0], 100, weights=weight[0], histtype='step', label='DoubleEMEnriched')
plt.hist(max_id[1], 100, weights=weight[1], histtype='step', label='TuneCUETP8M1')
plt.hist(max_id[2], 100, weights=weight[2], histtype='step', label='TuneCP5')
plt.xlabel('ID MVA')
plt.ylabel('Entries')
plt.legend(loc='upper left')
plt.title('Photons with MAX photonID')
plt.subplot(122)
plt.hist(min_id[0], 100, weights=weight[0], histtype='step', label='DoubleEMEnriched')
plt.hist(min_id[1], 100, weights=weight[1], histtype='step', label='TuneCUETP8M1')
plt.hist(min_id[2], 100, weights=weight[2], histtype='step', label='TuneCP5')
plt.xlabel('ID MVA')
plt.ylabel('Entries')
plt.legend(loc='upper left')
plt.title('Photons with MIN photonID')
plt.savefig('plots/GJet_photonID_VersionComparison.png')

plt.figure(2, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.subplot(121)
plt.hist(np.clip(maxid_pt[0], None, 401), 101, weights=weight[0], histtype='step', label='DoubleEMEnriched')
plt.hist(np.clip(maxid_pt[1], None, 401), 101, weights=weight[1], histtype='step', label='TuneCUETP8M1')
plt.hist(np.clip(maxid_pt[2], None, 401), 101, weights=weight[2], histtype='step', label='TuneCP5')
plt.xlabel('pT [GeV]')
plt.ylabel('Entries')
plt.yscale('log')
plt.legend()
plt.title('Photons with MAX photonID')
plt.subplot(122)
plt.hist(np.clip(minid_pt[0], None, 401), 101, weights=weight[0], histtype='step', label='DoubleEMEnriched')
plt.hist(np.clip(minid_pt[1], None, 401), 101, weights=weight[1], histtype='step', label='TuneCUETP8M1')
plt.hist(np.clip(minid_pt[2], None, 401), 101, weights=weight[2], histtype='step', label='TuneCP5')
plt.xlabel('pT [GeV]')
plt.ylabel('Entries')
plt.yscale('log')
plt.legend()
plt.title('Photons with MIN photonID')
plt.savefig('plots/GJet_pT_VersionComparison.png')

plt.figure(3, figsize=(1200 / my_dpi, 720 / my_dpi), dpi=my_dpi)
plt.subplot(121)
plt.hist(maxid_eta[0], 100, weights=weight[0], histtype='step', label='DoubleEMEnriched')
plt.hist(maxid_eta[1], 100, weights=weight[1], histtype='step', label='TuneCUETP8M1')
plt.hist(maxid_eta[2], 100, weights=weight[2], histtype='step', label='TuneCP5')
plt.xlabel('eta')
plt.ylabel('Entries')
plt.legend()
plt.title('Photons with MAX photonID')
plt.subplot(122)
plt.hist(minid_eta[0], 100, weights=weight[0], histtype='step', label='DoubleEMEnriched')
plt.hist(minid_eta[1], 100, weights=weight[1], histtype='step', label='TuneCUETP8M1')
plt.hist(minid_eta[2], 100, weights=weight[2], histtype='step', label='TuneCP5')
plt.xlabel('eta')
plt.ylabel('Entries')
plt.legend()
plt.title('Photons with MIN photonID')
plt.savefig('plots/GJet_eta_SampleComparison.png')

plt.show()
